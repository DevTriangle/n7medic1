package com.triangle.n7medic1.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

/*
Описание: Модель пациента
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
@Keep
data class User(
    @SerializedName("id") val id: Int,
    @SerializedName("firstname") val firstName: String,
    @SerializedName("middlename") val patronymic: String,
    @SerializedName("lastname") val lastName: String,
    @SerializedName("bith") val birthday: String,
    @SerializedName("pol") val gender: String,
    @SerializedName("image") val image: String? = null,
    @SerializedName("cart") val cart: MutableList<CartItem> = ArrayList<CartItem>(),
)
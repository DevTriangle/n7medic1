package com.triangle.n7medic1.model

import com.google.gson.annotations.SerializedName

/*
Описание: Модель анализа
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
data class Analysis(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("description") val description: String,
    @SerializedName("price") val price: String,
    @SerializedName("category") val category: String,
    @SerializedName("time_result") val timerResult: String,
    @SerializedName("preparation") val preparation: String,
    @SerializedName("bio") val bio: String,
)
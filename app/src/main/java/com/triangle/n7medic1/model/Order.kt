package com.triangle.n7medic1.model

import com.google.gson.annotations.SerializedName

/*
Описание: Модель заказа
Дата создания: 10.04.2023
Автор: Хасанов Альберт
 */
data class Order(
   @SerializedName("address") val address: String,
   @SerializedName("dateTime") val dateTime: String,
   @SerializedName("selectedPatients") val selectedPatients: MutableList<User>,
   @SerializedName("phone") val phone: String,
   @SerializedName("comment") val comment: String,
)

/*
Описание: Модель заказа для отправки на сервер
Дата создания: 10.04.2023
Автор: Хасанов Альберт
 */
data class ServerOrder(
   @SerializedName("address") val address: String,
   @SerializedName("date_time") val dateTime: String,
   @SerializedName("phone") val phone: String,
   @SerializedName("comment") val comment: String,
   @SerializedName("audio_comment") val audioComment: String,
   @SerializedName("patients") val patients: MutableList<OrderPatient>,
)

/*
Описание: Модель клиента в заказе
Дата создания: 10.04.2023
Автор: Хасанов Альберт
 */
data class OrderPatient(
   @SerializedName("name") val name: String,
   @SerializedName("items") val items: MutableList<OrderItem>,
)

/*
Описание: Модель товара в заказе
Дата создания: 10.04.2023
Автор: Хасанов Альберт
 */
data class OrderItem(
   @SerializedName("catalog_id") val id: Int,
   @SerializedName("price") val price: String,
)
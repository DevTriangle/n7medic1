package com.triangle.n7medic1.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

/*
Описание: Модель товара в корзине
Дата создания: 06.04.2023
Автор: Хасанов Альберт
 */
@Keep
data class CartItem(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("price") val price: String,
    @SerializedName("count") var count: Int
)
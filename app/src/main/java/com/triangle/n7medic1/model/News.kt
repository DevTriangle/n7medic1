package com.triangle.n7medic1.model

import com.google.gson.annotations.SerializedName

/*
Описание: Модель новости
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
data class News(
    @SerializedName("name") val name: String,
    @SerializedName("description") val description: String,
    @SerializedName("price") val price: String,
    @SerializedName("image") val image: String,
)

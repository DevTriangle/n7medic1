package com.triangle.n7medic1.model

import com.google.gson.annotations.SerializedName

/*
Описание: Модель адреса пользователя
Дата создания: 10.04.2023
Автор: Хасанов Альберт
*/
data class AppAddress(
    @SerializedName("name") val name: String,
    @SerializedName("address") val address: String,
    @SerializedName("lat") val lat: String,
    @SerializedName("lon") val lon: String,
    @SerializedName("alt") val alt: String,
    @SerializedName("flat") val flat: String,
    @SerializedName("entrance") val entrance: String,
    @SerializedName("floor") val floor: String,
    @SerializedName("doorPhone") val doorPhone: String,
)

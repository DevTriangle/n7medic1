package com.triangle.n7medic1.view

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.lifecycle.ViewModelProvider
import com.triangle.n7medic1.viewmodel.LoginViewModel
import com.triangle.n7medic1.R
import com.triangle.n7medic1.common.DataSaver
import com.triangle.n7medic1.common.EncryptDataSaver
import com.triangle.n7medic1.ui.components.AppTextButton
import com.triangle.n7medic1.ui.theme.*
import com.triangle.n7medic1.view.ui.components.AppTextField
import com.triangle.n7medic1.view.ui.components.LoadingDialog
import kotlinx.coroutines.delay

/*
Описание: Активити экрана ввода кода из email
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
class CodeActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            N7medic1Theme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background,
                ) {
                    ScreenContent()
                }
            }
        }
    }

    /*
    Описание: Содержание экрана ввода кода из email
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    @Composable
    fun ScreenContent() {
        val mContext = LocalContext.current
        val viewModel = ViewModelProvider(this)[LoginViewModel::class.java]

        var code1 by rememberSaveable { mutableStateOf("") }
        var code2 by rememberSaveable { mutableStateOf("") }
        var code3 by rememberSaveable { mutableStateOf("") }
        var code4 by rememberSaveable { mutableStateOf("") }

        var isLoading by rememberSaveable { mutableStateOf(false) }
        var isErrorVisible by rememberSaveable { mutableStateOf(false) }

        val email = intent.getStringExtra("email")!!

        var timer by rememberSaveable { mutableStateOf(60) }
        LaunchedEffect(timer) {
            delay(1000)

            if (timer > 0) {
                timer--
            } else {
                timer = 60
                isLoading = true
                viewModel.sendCode(email)
            }
        }

        val isSuccess by viewModel.isSuccessSendCode.observeAsState()
        LaunchedEffect(isSuccess) {
            if (isSuccess == true) {
                isLoading = false
            } else {
                isLoading = false
            }
        }

        val errorMessage by viewModel.message.observeAsState()
        LaunchedEffect(errorMessage) {
            if (errorMessage != null) {
                isLoading = false
                isErrorVisible = true
            }
        }

        val token by viewModel.token.observeAsState()
        LaunchedEffect(token) {
            if (token != null) {
                EncryptDataSaver(application).saveToken(token!!)

                isLoading = false

                val intent = Intent(mContext, PasswordActivity::class.java)
                startActivity(intent)
            }
        }

        Scaffold(topBar = {
            Box(
                modifier = Modifier
                    .padding(start = 20.dp, top = 24.dp)
                    .size(32.dp)
                    .clip(MaterialTheme.shapes.medium)
                    .background(inputBG)
                    .clickable { onBackPressed() }
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_back),
                    contentDescription = "",
                    modifier = Modifier
                        .align(Alignment.Center),
                    tint = descriptionColor
                )
            }
        }) {
            Box(modifier = Modifier.padding(it)) {
                Column(
                    modifier = Modifier.fillMaxSize(),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Column(
                        modifier = Modifier
                            .widthIn(max = 400.dp)
                            .fillMaxWidth(),
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Text(
                            text = "Введите код из E-mail",
                            fontWeight = FontWeight.SemiBold,
                            fontSize = 17.sp,
                            fontFamily = SSPro,
                            textAlign = TextAlign.Center
                        )
                        Spacer(modifier = Modifier.height(24.dp))
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.Center
                        ) {
                            AppTextField(
                                value = code1,
                                onValueChange = { code ->
                                    if (code.length < 2) code1 = code
                                },
                                changeBorder = false,
                                textStyle = TextStyle(
                                    fontFamily = SSPro,
                                    fontSize = 20.sp,
                                    textAlign = TextAlign.Center
                                ),
                                contentPadding = PaddingValues(10.dp),
                                modifier = Modifier
                                    .padding(end = 16.dp)
                                    .size(48.dp),
                                singleLine = true
                            )
                            AppTextField(
                                value = code2,
                                onValueChange = { code ->
                                    if (code.length < 2) code2 = code
                                },
                                changeBorder = false,
                                textStyle = TextStyle(
                                    fontFamily = SSPro,
                                    fontSize = 20.sp,
                                    textAlign = TextAlign.Center
                                ),
                                contentPadding = PaddingValues(10.dp),
                                modifier = Modifier
                                    .padding(end = 16.dp)
                                    .size(48.dp),
                                singleLine = true
                            )
                            AppTextField(
                                value = code3,
                                onValueChange = { code ->
                                    if (code.length < 2) code3 = code
                                },
                                changeBorder = false,
                                textStyle = TextStyle(
                                    fontFamily = SSPro,
                                    fontSize = 20.sp,
                                    textAlign = TextAlign.Center
                                ),
                                contentPadding = PaddingValues(10.dp),
                                modifier = Modifier
                                    .padding(end = 16.dp)
                                    .size(48.dp),
                                singleLine = true
                            )
                            AppTextField(
                                value = code4,
                                onValueChange = { code ->
                                    if (code.length < 2) code4 = code

                                    if (code.length == 1) {
                                        isLoading = true
                                        viewModel.signIn(email, "${code1}${code2}${code3}${code4}")
                                    }
                                },
                                changeBorder = false,
                                textStyle = TextStyle(
                                    fontFamily = SSPro,
                                    fontSize = 20.sp,
                                    textAlign = TextAlign.Center
                                ),
                                singleLine = true,
                                contentPadding = PaddingValues(10.dp),
                                modifier = Modifier
                                    .size(48.dp)
                            )
                        }
                        Spacer(modifier = Modifier.height(16.dp))
                        Text(
                            text = "Отправить код повторно можно будет через $timer секунд",
                            fontSize = 15.sp,
                            fontFamily = SSPro,
                            color = captionColor,
                            modifier = Modifier
                                .width(242.dp),
                            textAlign = TextAlign.Center
                        )
                    }
                }
            }
        }

        if (isLoading) {
            LoadingDialog()
        }

        if (isErrorVisible) {
            AlertDialog(
                onDismissRequest = { isErrorVisible = false },
                title = {
                    Text(text = "Ошибка", fontSize = 18.sp, fontWeight = FontWeight.SemiBold)
                },
                text = {
                    Text(text = errorMessage!!, fontSize = 16.sp)
                },
                buttons = {
                    AppTextButton(
                        onClick = {
                            isErrorVisible = false
                        },
                        label = "Ок"
                    )
                }
            )
        }
    }
}
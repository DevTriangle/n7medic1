package com.triangle.n7medic1.view.ui.components

import android.service.autofill.OnClickAction
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.triangle.n7medic1.model.Analysis
import com.triangle.n7medic1.ui.components.AppButton
import com.triangle.n7medic1.ui.theme.SSPro
import com.triangle.n7medic1.ui.theme.captionColor
import com.triangle.n7medic1.ui.theme.primary

/*
Описание: Компонент анализа в каталоге
Дата создания: 04.04.2023
Автор: Хасанов Альберт
*/
@Composable
fun CatalogCard(
    modifier: Modifier = Modifier,
    analysis: Analysis,
    onButtonClick: (Analysis) -> Unit,
    onCardClick: (Analysis) -> Unit,
    isInCart: Boolean
) {
    Box(
        modifier = modifier
            .padding(top = 16.dp)
            .shadow(10.dp, shape = MaterialTheme.shapes.large, spotColor = Color.Black.copy(0.2f))
            .clip(MaterialTheme.shapes.large)
            .background(Color.White)
            .clickable { onCardClick(analysis) }
    ) {
        Column(modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)) {
            Text(
                text = analysis.name,
                fontFamily = SSPro,
                fontSize = 16.sp
            )
            Spacer(modifier = Modifier.height(16.dp))
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column() {
                    Text(
                        text = analysis.timerResult,
                        color = captionColor,
                        fontWeight = FontWeight.SemiBold,
                        fontFamily = SSPro,
                        fontSize = 14.sp
                    )
                    Spacer(modifier = Modifier.height(4.dp))
                    Text(
                        text = analysis.price + " ₽",
                        fontWeight = FontWeight.SemiBold,
                        fontFamily = SSPro,
                        fontSize = 17.sp
                    )
                }
                AppButton(
                    onClick = { onButtonClick(analysis) },
                    label = if (isInCart) "Убрать" else "Добавить",
                    style = TextStyle(
                        fontSize = 14.sp,
                        fontWeight = FontWeight.SemiBold
                    ),
                    contentPadding = PaddingValues(17.dp, 10.dp),
                    modifier = Modifier.width(124.dp),
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = if (isInCart) Color.White else primary,
                        contentColor = if (isInCart) primary else Color.White,
                    ),
                    border = BorderStroke(1.dp, primary)
                )
            }
        }
    }
}
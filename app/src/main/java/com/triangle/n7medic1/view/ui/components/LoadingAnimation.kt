package com.triangle.n7medic1.view.ui.components

import androidx.compose.animation.Animatable
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.res.painterResource
import com.triangle.n7medic1.R

/*
Описание: Индикация загрузки
Дата создания: 10.04.2023
Автор: Хасанов Альберт
 */
@Composable
fun LoadingAnimation() {
    val rotation = remember {
        androidx.compose.animation.core.Animatable(0f)
    }
    
    LaunchedEffect(rotation) {
        rotation.animateTo(
            360f,
            animationSpec = infiniteRepeatable(
                animation = tween(1000, easing = LinearEasing),
                repeatMode = RepeatMode.Restart
            )
        )
    }
    
    Image(
        painter = painterResource(R.drawable.loading),
        contentDescription = "",
        modifier = Modifier
            .rotate(rotation.value)
    )
}
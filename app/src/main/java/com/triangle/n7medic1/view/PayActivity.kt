package com.triangle.n7medic1.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Space
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.accompanist.flowlayout.FlowRow
import com.google.accompanist.flowlayout.MainAxisAlignment
import com.triangle.n7medic1.R
import com.triangle.n7medic1.common.EncryptDataSaver
import com.triangle.n7medic1.model.OrderItem
import com.triangle.n7medic1.model.OrderPatient
import com.triangle.n7medic1.model.ServerOrder
import com.triangle.n7medic1.ui.components.AppButton
import com.triangle.n7medic1.ui.theme.*
import com.triangle.n7medic1.view.ui.components.LoadingAnimation
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/*
Описание: Экран оформление заказа
Дата создания: 10.04.2023
Автор: Хасанов Альберт
 */
class PayActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            N7medic1Theme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    ScreenContent()
                }
            }
        }
    }

    /*
    Описание: Содержание экрана оформление заказа
    Дата создания: 10.04.2023
    Автор: Хасанов Альберт
     */
    @Composable
    fun ScreenContent() {
        val mContext = LocalContext.current
        var isLoading by rememberSaveable { mutableStateOf(true) }
        var loadingText by rememberSaveable { mutableStateOf("Связываемся с банком...") }

        LaunchedEffect(Unit) {
            delay(1500)
            loadingText = "Производим оплату..."
            delay(1500)
            isLoading = false
        }

        Column(modifier = Modifier.fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally) {
            Column(
                modifier = Modifier.fillMaxSize().padding(20.dp),
                verticalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = "Оплата",
                    modifier = Modifier.fillMaxWidth(),
                    textAlign = TextAlign.Center,
                    fontWeight = FontWeight.SemiBold,
                    fontSize = 20.sp
                )
                AnimatedVisibility(visible = isLoading) {
                    Column(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center
                    ) {
                        LoadingAnimation()
                        Text(
                            text = loadingText,
                            color = captionColor,
                            fontSize = 16.sp
                        )
                    }
                }
                AnimatedVisibility(visible = !isLoading) {
                    Column(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center
                    ) {
                        Image(
                            painter = painterResource(id = R.drawable.onboard_1),
                            contentDescription = ""
                        )
                        Spacer(modifier = Modifier.height(30.dp))
                        Text(
                            text = "Ваш заказ успешно оплачен!",
                            style = TextStyle(
                                fontSize = 20.sp,
                                fontWeight = FontWeight.Bold,
                                textAlign = TextAlign.Center,
                                color = onboardTitle
                            )
                        )
                        Spacer(modifier = Modifier.height(10.dp))
                        Text(
                            text = "Вам осталось дождаться приезда медсестры и сдать анализы. До скорой встречи!",
                            style = TextStyle(
                                fontSize = 14.sp,
                                fontFamily = SSPro,
                                textAlign = TextAlign.Center,
                                color = captionColor
                            )
                        )
                        Spacer(modifier = Modifier.height(10.dp))
                        FlowRow(Modifier.fillMaxWidth(), mainAxisAlignment = MainAxisAlignment.Center) {
                            Text(
                                text = "Не забудьте ознакомиться с",
                                style = TextStyle(
                                    fontSize = 14.sp,
                                    fontFamily = SSPro,
                                    textAlign = TextAlign.Center,
                                    color = captionColor
                                )
                            )
                            Spacer(modifier = Modifier.width(2.dp))
                            Icon(
                                painter = painterResource(id = R.drawable.file),
                                contentDescription = ""
                            )
                            Spacer(modifier = Modifier.width(2.dp))
                            Text(
                                text = "правилами подготовки к сдаче анализов",
                                style = TextStyle(
                                    fontSize = 14.sp,
                                    fontFamily = SSPro,
                                    textAlign = TextAlign.Center,
                                    color = primary
                                ),
                                modifier = Modifier
                                    .clickable {
                                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://medic.madskill.ru/avatar/prav.pdf"))
                                        startActivity(intent)
                                    }
                            )
                        }
                    }
                }
                if (isLoading) {
                    Spacer(modifier = Modifier.height(1.dp))
                } else {
                    Column(modifier = Modifier.fillMaxWidth()) {
                        AppButton(
                            onClick = {},
                            label = "Чек покупки",
                            border = BorderStroke(1.dp, primary),
                            style = TextStyle(
                                fontWeight = FontWeight.SemiBold
                            ),
                            colors = ButtonDefaults.buttonColors(
                                backgroundColor = Color.White,
                                contentColor = primary
                            )
                        )
                        Spacer(modifier = Modifier.height(20.dp))
                        AppButton(
                            onClick = {
                                val intent = Intent(mContext, HomeActivity::class.java)
                                startActivity(intent)
                            },
                            label = "На главную",
                        )
                    }
                }
            }
        }
    }
}
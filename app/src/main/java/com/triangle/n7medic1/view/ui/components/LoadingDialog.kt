package com.triangle.n7medic1.view.ui.components

import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.window.Dialog

/*
Описание: Индикация загрузки
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
@Composable
fun LoadingDialog() {
    Dialog(onDismissRequest = {}) {
        CircularProgressIndicator()
    }
}
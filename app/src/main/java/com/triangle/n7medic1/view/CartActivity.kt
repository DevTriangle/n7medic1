package com.triangle.n7medic1.view

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.BackHandler
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.triangle.n7medic1.R
import com.triangle.n7medic1.common.CartService
import com.triangle.n7medic1.model.CartItem
import com.triangle.n7medic1.ui.components.AppButton
import com.triangle.n7medic1.ui.theme.*
import com.triangle.n7medic1.view.ui.components.CartComponent

/*
Описание: Активити корзины
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
class CartActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            N7medic1Theme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    ScreenContent()
                }
            }
        }
    }

    /*
    Описание: Содеражание экрана корзины
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    @Composable
    fun ScreenContent() {
        val mContext = LocalContext.current
        val sharedPreferences = mContext.getSharedPreferences("shared", MODE_PRIVATE)

        val cart: MutableList<CartItem> = remember { mutableStateListOf() }
        LaunchedEffect(Unit) {
            cart.addAll(CartService().loadCart(sharedPreferences))
        }

        BackHandler() {
            val intent = Intent(mContext, HomeActivity::class.java)
            startActivity(intent)
        }

        Scaffold(
            topBar = {
                Column(modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 20.dp, top = 16.dp)
                    .background(Color.White)) {
                    Box(
                        modifier = Modifier
                            .size(32.dp)
                            .clip(MaterialTheme.shapes.medium)
                            .background(inputBG)
                            .clickable {
                                val intent = Intent(mContext, HomeActivity::class.java)
                                startActivity(intent)
                            }
                    ) {
                        Icon(
                            painter = painterResource(id = R.drawable.ic_back),
                            contentDescription = "",
                            modifier = Modifier
                                .align(Alignment.Center),
                            tint = descriptionColor
                        )
                    }
                    Spacer(modifier = Modifier.height(height = 24.dp))
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceBetween,
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text(
                            text = "Корзина",
                            fontSize = 24.sp,
                            fontWeight = FontWeight.Bold
                        )
                        IconButton(
                            onClick = {
                                cart.clear()
                                CartService().saveCart(sharedPreferences, cart)
                            },
                        ) {
                            Icon(
                                painter = painterResource(id = R.drawable.ic_remove),
                                contentDescription = "",
                                tint = iconsColor,
                                modifier = Modifier
                                    .size(20.dp)
                            )
                        }
                    }
                }
            }
        ) {
            Box(modifier = Modifier.padding(it)) {
                Column(modifier = Modifier
                    .fillMaxSize()
                    .padding(20.dp), verticalArrangement = Arrangement.SpaceBetween) {
                    Column(
                        modifier = Modifier
                            .fillMaxWidth(),
                    ) {
                        LazyColumn() {
                            items (items = cart.distinct()) { item ->
                                CartComponent(
                                    cartItem = item,
                                    onRemoveClick = { i ->
                                        cart.remove(i)
                                        CartService().saveCart(sharedPreferences, cart)
                                    },
                                    onAddClick = { c ->
                                        val index = cart.indexOf(c)

                                        cart.removeAt(index)
                                        cart.add(
                                            index,
                                            CartItem(
                                                c.id,
                                                c.name,
                                                c.price,
                                                c.count + 1
                                            )
                                        )

                                        CartService().saveCart(sharedPreferences, cart)
                                    },
                                    onMinusClick = { c ->
                                        val index = cart.indexOf(c)

                                        cart.removeAt(index)
                                        cart.add(
                                            index,
                                            CartItem(
                                                c.id,
                                                c.name,
                                                c.price,
                                                c.count - 1
                                            )
                                        )

                                        CartService().saveCart(sharedPreferences, cart)
                                    }
                                )
                            }
                        }
                        Spacer(modifier = Modifier.height(40.dp))
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceBetween,
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            var sumPrice = 0

                            for (item in cart) {
                                sumPrice += item.price.toInt() * item.count
                            }

                            Text(
                                text = "Сумма",
                                fontFamily = SSPro,
                                fontSize = 20.sp,
                                fontWeight = FontWeight.SemiBold
                            )
                            Text(
                                text = "$sumPrice ₽",
                                fontFamily = SSPro,
                                fontSize = 20.sp,
                                fontWeight = FontWeight.SemiBold
                            )
                        }
                    }
                    AppButton(
                        onClick = {
                            val intent = Intent(mContext, OrderActivity::class.java)
                            startActivity(intent)
                        },
                        label = "Перейти к оформлению заказа",
                    )
                }
            }
        }
    }
}
package com.triangle.n7medic1.view.ui.components

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.widget.Space
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.triangle.n7medic1.R
import com.triangle.n7medic1.ui.components.AppButton
import com.triangle.n7medic1.ui.theme.*
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.Calendar

/*
Описание: Экран выбора времени
Дата создания: 10.04.2023
Автор: Хасанов Альберт
 */
@SuppressLint("SimpleDateFormat")
@Composable
fun TimeBottomSheet(
    onDateTimeSelect: (String, LocalDateTime) -> Unit
) {
    val mContext = LocalContext.current

    var displayDate by rememberSaveable { mutableStateOf("") }

    val times = listOf(
        "10:00",
        "13:00",
        "14:00",
        "15:00",
        "16:00",
        "18:00",
        "19:00"
    )
    var selectedTime by rememberSaveable {
        mutableStateOf(times[1])
    }

    val sdf = SimpleDateFormat("dd.MM.yyyy")
    val displayFormat = SimpleDateFormat("d")

    val calendar = Calendar.getInstance()

    val mYear = calendar.get(Calendar.YEAR)
    val mMonth = calendar.get(Calendar.MONTH)
    val mDay = calendar.get(Calendar.DAY_OF_MONTH)

    val datePicker = DatePickerDialog(
        mContext,
        { _, y: Int, m: Int, d: Int ->
            val date = sdf.parse("$d.$m.$y")

            var month = ""
            when(m) {
                0 -> { month = "января" }
                1 -> { month = "февраля" }
                2 -> { month = "марта" }
                3 -> { month = "апреля" }
                4 -> { month = "мая" }
                5 -> { month = "июня" }
                6 -> { month = "июля" }
                7 -> { month = "августа" }
                8 -> { month = "сентября" }
                9 -> { month = "октября" }
                10 -> { month = "ноября" }
                11 -> { month = "декабря" }
            }

            if (LocalDateTime.now().dayOfMonth == d) {
                displayDate = "Сегодня, ${displayFormat.format(date)} $month"
            } else {
                displayDate = "${displayFormat.format(date)} $month"
            }
        }, mYear, mMonth, mDay
    )

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(20.dp, 24.dp)
    ) {
        Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
            Text(
                text = "Дата и время",
                fontFamily = SSPro,
                fontSize = 20.sp,
                fontWeight = FontWeight.SemiBold,
                modifier = Modifier.fillMaxWidth(0.7f)
            )
            Box(
                modifier = Modifier
                    .size(24.dp)
                    .clip(CircleShape)
                    .background(inputBG)
                    .clickable {}
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_close),
                    contentDescription = "",
                    modifier = Modifier
                        .align(Alignment.Center),
                    tint = descriptionColor
                )
            }
        }
        Spacer(modifier = Modifier.height(24.dp))
        Text(
            text = "Выберите дату",
            fontSize = 16.sp,
            color = captionColor
        )
        Spacer(modifier = Modifier.height(16.dp))
        AppTextField(
            value = displayDate,
            onValueChange = {},
            changeBorder = false,
            trailingIcon = {
                IconButton(onClick = { datePicker.show() }) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_dropdown),
                        contentDescription = "",
                        tint = descriptionColor,
                    )
                }
            },
            modifier = Modifier
                .fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(32.dp))
        Text(
            text = "Выберите время",
            fontSize = 16.sp,
            color = captionColor
        )
        Spacer(modifier = Modifier.height(16.dp))
        LazyVerticalGrid(
            columns = GridCells.Adaptive(70.dp),
            horizontalArrangement = Arrangement.spacedBy(16.dp),
            verticalArrangement = Arrangement.spacedBy(16.dp),
            content = {
                items(items = times) { t ->
                    Box(
                        modifier = Modifier
                            .width(70.dp)
                            .height(40.dp)
                            .clip(MaterialTheme.shapes.medium)
                            .background(if (selectedTime == t) primary else inputBG)
                            .clickable {
                                selectedTime = t
                            }
                    ) {
                        Text(
                            text = t,
                            fontSize = 16.sp,
                            color = if (selectedTime == t) Color.White else descriptionColor,
                            modifier = Modifier
                                .align(Alignment.Center)
                        )
                    }
                }
            }
        )
        Spacer(modifier = Modifier.height(48.dp))
        AppButton(
            onClick = {
                onDateTimeSelect("${displayDate} $selectedTime", LocalDateTime.now())
            },
            label = "Подтвердить"
        )
    }
}
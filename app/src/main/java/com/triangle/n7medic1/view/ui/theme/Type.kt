package com.triangle.n7medic1.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.triangle.n7medic1.R

val Lato = FontFamily(
    Font(R.font.lato_bold, FontWeight.Bold)
)

val SSPro = FontFamily(
    Font(R.font.ss_pro_normal, FontWeight.Normal),
    Font(R.font.ss_pro_semibold, FontWeight.SemiBold),
    Font(R.font.ss_pro_bold, FontWeight.Bold),
)

val Typography = Typography(
    body1 = TextStyle(
        fontFamily = SSPro,
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp
    )
)
package com.triangle.n7medic1.view.screens

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.provider.ContactsContract.Profile
import android.provider.MediaStore
import android.provider.MediaStore.Audio.Media
import androidx.activity.result.ActivityResultLauncher
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.app.ActivityCompat
import androidx.core.net.toUri
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.glide.GlideImage
import com.triangle.n7medic1.viewmodel.UserViewModel
import com.triangle.n7medic1.R
import com.triangle.n7medic1.common.DataSaver
import com.triangle.n7medic1.common.EncryptDataSaver
import com.triangle.n7medic1.model.User
import com.triangle.n7medic1.ui.components.AppButton
import com.triangle.n7medic1.ui.components.AppTextButton
import com.triangle.n7medic1.ui.theme.SSPro
import com.triangle.n7medic1.ui.theme.captionColor
import com.triangle.n7medic1.ui.theme.descriptionColor
import com.triangle.n7medic1.view.ManageCardActivity
import com.triangle.n7medic1.view.ui.components.AppTextField
import com.triangle.n7medic1.view.ui.components.LoadingDialog
import java.io.File

/*
Описание: Текстовая кнопка
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ProfileScreen(
    viewModel: UserViewModel,
    application: Application,
    imageResultLauncher: ActivityResultLauncher<Intent>,
    videoResultLauncher: ActivityResultLauncher<Intent>
) {
    val mContext = LocalContext.current
    val sharedPreferences = mContext.getSharedPreferences("shared", Context.MODE_PRIVATE)

    var firstName by rememberSaveable { mutableStateOf("") }
    var lastName by rememberSaveable { mutableStateOf("") }
    var patronymic by rememberSaveable { mutableStateOf("") }
    var birthday by rememberSaveable { mutableStateOf("") }
    var gender by rememberSaveable { mutableStateOf("") }

    var isExpanded by rememberSaveable { mutableStateOf(false) }

    var isLoading by rememberSaveable { mutableStateOf(false) }
    var isErrorVisible by rememberSaveable { mutableStateOf(false) }
    var profilePicVisible by rememberSaveable { mutableStateOf(false) }

    val errorMessage by viewModel.message.observeAsState()
    LaunchedEffect(errorMessage) {
        if (errorMessage != null) {
            isLoading = false
            isErrorVisible = true
        }
    }

    val userList: MutableList<User> = remember { mutableStateListOf() }
    LaunchedEffect(Unit) {
        userList.addAll(DataSaver().loadUsers(sharedPreferences))

        if (userList.isEmpty()) {
            val intent = Intent(mContext, ManageCardActivity::class.java)
            mContext.startActivity(intent)
        } else {
            firstName = userList[0].firstName
            lastName = userList[0].lastName
            patronymic = userList[0].patronymic
            birthday = userList[0].birthday
            gender = userList[0].gender
        }
    }

    val image by viewModel.image.observeAsState()
    val video by viewModel.video.observeAsState()
    val isVideo by viewModel.isVideo.observeAsState()

    val newUser by viewModel.newUser.observeAsState()
    LaunchedEffect(newUser) {
        if (newUser != null) {
            isLoading = false
            userList.remove(userList[0])
            userList.add(0, newUser!!)
        }
    }

    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Column(
            modifier = Modifier
                .widthIn(max = 400.dp)
                .padding(horizontal = 20.dp)
                .fillMaxSize(),
        ) {
            Spacer(modifier = Modifier.height(17.dp))
            Text(
                text = "Карта пациента",
                fontWeight = FontWeight.Bold,
                fontSize = 24.sp,
                textAlign = TextAlign.Center,
                fontFamily = SSPro,
                modifier = Modifier
                    .fillMaxWidth()
            )
            Spacer(modifier = Modifier.height(7.dp))
            Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
                if (isVideo == false) {
                    GlideImage(
                        imageModel = image,
                        imageOptions = ImageOptions(
                            Alignment.Center,
                            contentScale = ContentScale.Crop
                        ),
                        failure = {
                            Box(modifier = Modifier.fillMaxSize()) {
                                Image(
                                    painter = painterResource(id = R.drawable.ic_photo),
                                    contentDescription = "",
                                    modifier = Modifier
                                        .align(Alignment.Center)
                                        .width(53.dp),
                                    contentScale = ContentScale.Fit
                                )
                            }
                        },
                        modifier = Modifier
                            .width(148.dp)
                            .height(123.dp)
                            .clip(CircleShape)
                            .background(Color(0xFFD9D9D9))
                            .clickable {
                                profilePicVisible = true
                            }
                    )
                } else {
                    Box(
                        modifier = Modifier
                            .width(148.dp)
                            .height(123.dp)
                            .clip(CircleShape)
                            .clickable {
                                profilePicVisible = true
                            }
                    ) {
                        val exoPlayer = ExoPlayer.Builder(mContext).build().apply {
                            val dataSourceFactory = DefaultDataSourceFactory(mContext, mContext.packageName)
                            val source = ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(
                                MediaItem.fromUri(video.toString()))

                            prepare(source)
                        }

                        AndroidView({
                            PlayerView(it).apply {
                                player = exoPlayer
                                exoPlayer.repeatMode = Player.REPEAT_MODE_ALL
                                useController = false
                                exoPlayer.play()
                            }
                        },
                        modifier = Modifier.align(Alignment.Center))
                    }
                }
            }
            Spacer(modifier = Modifier.height(7.dp))
            Text(
                text = "Без карты пациента вы не сможете заказать анализы.",
                color = captionColor,
                fontSize = 14.sp,
                fontFamily = SSPro
            )
            Spacer(modifier = Modifier.height(8.dp))
            Text(
                text = "В картах пациентов будут храниться результаты анализов вас и ваших близких.",
                color = captionColor,
                fontSize = 14.sp,
                fontFamily = SSPro
            )
            Spacer(modifier = Modifier.height(8.dp))
            AppTextField(
                value = firstName,
                onValueChange = {
                    firstName = it
                },
                changeBorder = true,
                placeholder = {
                    Text(
                        text = "Имя",
                        fontFamily = SSPro,
                        fontSize = 15.sp,
                        color = captionColor
                    )
                },
                modifier = Modifier.fillMaxWidth()
            )
            Spacer(modifier = Modifier.height(24.dp))
            AppTextField(
                value = patronymic,
                onValueChange = {
                    patronymic = it
                },
                placeholder = {
                    Text(
                        text = "Отчество",
                        fontFamily = SSPro,
                        fontSize = 15.sp,
                        color = captionColor
                    )
                },
                changeBorder = true,
                modifier = Modifier.fillMaxWidth()
            )
            Spacer(modifier = Modifier.height(24.dp))
            AppTextField(
                value = lastName,
                onValueChange = {
                    lastName = it
                },
                placeholder = {
                    Text(
                        text = "Фамилия",
                        fontFamily = SSPro,
                        fontSize = 15.sp,
                        color = captionColor
                    )
                },
                changeBorder = true,
                modifier = Modifier.fillMaxWidth()
            )
            Spacer(modifier = Modifier.height(24.dp))
            AppTextField(
                value = birthday,
                onValueChange = {
                    birthday = it
                },
                placeholder = {
                    Text(
                        text = "Дата рождения",
                        fontFamily = SSPro,
                        fontSize = 15.sp,
                        color = captionColor
                    )
                },
                changeBorder = true,
                modifier = Modifier.fillMaxWidth()
            )
            Spacer(modifier = Modifier.height(24.dp))
            ExposedDropdownMenuBox(
                expanded = isExpanded,
                onExpandedChange = {
                    isExpanded = it
                }
            ) {
                AppTextField(
                    value = gender,
                    onValueChange = {
                        gender = it
                    },
                    placeholder = {
                        Text(
                            text = "Пол",
                            fontFamily = SSPro,
                            fontSize = 15.sp,
                            color = captionColor
                        )
                    },
                    readOnly = true,
                    trailingIcon = {
                        IconButton(onClick = { isExpanded = true }) {
                            Icon(
                                painter = painterResource(id = R.drawable.ic_dropdown),
                                contentDescription = "",
                                tint = descriptionColor,
                            )
                        }
                    },
                    changeBorder = false,
                    modifier = Modifier.fillMaxWidth()
                )
                DropdownMenu(expanded = isExpanded, onDismissRequest = { isExpanded = false }) {
                    DropdownMenuItem(onClick = {
                        gender = "Мужской"
                        isExpanded = false
                    }) {
                        Text(text = "Мужской")
                    }
                    DropdownMenuItem(onClick = {
                        gender = "Женский"
                        isExpanded = false
                    }) {
                        Text(text = "Женский")
                    }
                }
            }
            Spacer(modifier = Modifier.height(22.dp))
            AppButton(
                onClick = {
                    isLoading = true
                    viewModel.updateProfile(
                        firstName,
                        patronymic,
                        lastName,
                        birthday,
                        gender,
                        EncryptDataSaver(application).getToken()
                    )
                },
                label = "Сохранить",
            )
        }
    }

    if (isLoading) {
        LoadingDialog()
    }

    if (isErrorVisible) {
        AlertDialog(
            onDismissRequest = { isErrorVisible = false },
            title = {
                Text(text = "Ошибка", fontSize = 18.sp, fontWeight = FontWeight.SemiBold)
            },
            text = {
                Text(text = errorMessage!!, fontSize = 16.sp)
            },
            buttons = {
                AppTextButton(
                    onClick = {
                        isErrorVisible = false
                    },
                    label = "Ок"
                )
            }
        )
    }

    if (profilePicVisible) {
        AlertDialog(
            onDismissRequest = { profilePicVisible = false },
            title = {
                Text(text = "Выбор", fontSize = 18.sp, fontWeight = FontWeight.SemiBold)
            },
            buttons = {
                Row(modifier = Modifier.fillMaxWidth()) {
                    AppTextButton(
                        onClick = {
                            profilePicVisible = false

                            val imageIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                            imageResultLauncher.launch(imageIntent, null)
                        },
                        label = "Фото"
                    )
                    Spacer(modifier = Modifier.width(16.dp))
                    AppTextButton(
                        onClick = {
                            profilePicVisible = false

                            val videoIntent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
                            videoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 3)
                            videoResultLauncher.launch(videoIntent, null)
                        },
                        label = "Видео"
                    )
                }
            }
        )
    }
}
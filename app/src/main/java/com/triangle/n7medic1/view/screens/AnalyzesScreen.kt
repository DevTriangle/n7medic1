package com.triangle.n7medic1.view.screens

import android.content.Context.MODE_PRIVATE
import android.content.Intent
import android.widget.Space
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsPressedAsState
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.DefaultStrokeLineMiter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import com.triangle.n7medic1.R
import com.triangle.n7medic1.common.CartService
import com.triangle.n7medic1.model.Analysis
import com.triangle.n7medic1.model.CartItem
import com.triangle.n7medic1.model.News
import com.triangle.n7medic1.ui.components.AppButton
import com.triangle.n7medic1.ui.components.AppTextButton
import com.triangle.n7medic1.ui.theme.*
import com.triangle.n7medic1.view.CartActivity
import com.triangle.n7medic1.view.CodeActivity
import com.triangle.n7medic1.view.ui.components.*
import com.triangle.n7medic1.viewmodel.HomeViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

/*
Описание: Экран Анализы/главная
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun AnalyzesScreen(
    viewModel: HomeViewModel
) {
    val mContext = LocalContext.current
    var searchText by rememberSaveable { mutableStateOf("") }
    val sharedPreferences = mContext.getSharedPreferences("shared", MODE_PRIVATE)

    val scope = rememberCoroutineScope()
    val sheetState = rememberModalBottomSheetState(initialValue = ModalBottomSheetValue.Hidden, skipHalfExpanded = true)

    val cart: MutableList<CartItem> = remember { mutableStateListOf() }
    LaunchedEffect(Unit) {
        cart.addAll(CartService().loadCart(sharedPreferences))
    }

    var isSearchEnabled by rememberSaveable { mutableStateOf(false) }

    val interactionSource = remember { MutableInteractionSource() }
    if (interactionSource.collectIsPressedAsState().value) {
        isSearchEnabled = true
    }

    var isLoading by rememberSaveable { mutableStateOf(false) }
    var isErrorVisible by rememberSaveable { mutableStateOf(false) }
    var isNewsVisible by rememberSaveable { mutableStateOf(true) }
    var selectedCategory by rememberSaveable { mutableStateOf("Популярные") }

    var selectedAnalysis: Analysis? by remember { mutableStateOf(null) }

    val scrollState = rememberScrollState()
    LaunchedEffect(scrollState) {
        snapshotFlow { scrollState.value }.collect() {
            isNewsVisible = it <= 0
        }
    }

    val isSuccessLoadNews by viewModel.isSuccessLoadNews.observeAsState()

    val isSuccessLoadCatalog by viewModel.isSuccessLoadCatalog.observeAsState()
    LaunchedEffect(isSuccessLoadNews) {
        if (isSuccessLoadNews == true && isSuccessLoadCatalog == true) {
            isLoading = false
        }
    }

    LaunchedEffect(isSuccessLoadCatalog) {
        if (isSuccessLoadCatalog == true && isSuccessLoadNews == true) {
            isLoading = false
        }
    }

    val errorMessage by viewModel.message.observeAsState()
    LaunchedEffect(errorMessage) {
        if (errorMessage != null) {
            isLoading = false
            isErrorVisible = true
        }
    }

    LaunchedEffect(Unit) {
        isLoading = true
        viewModel.loadNews()
        viewModel.loadCatalog()

        if (cart.isNotEmpty()) {
            delay(1000)
            scrollState.scrollTo(10)
        }
    }

    ModalBottomSheetLayout(
        sheetState = sheetState,
        sheetShape = MaterialTheme.shapes.large,
        sheetContent = {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(20.dp, 24.dp)
                    .verticalScroll(rememberScrollState()),
                verticalArrangement = Arrangement.SpaceBetween
            ) {
                if (selectedAnalysis != null) {
                    Column(modifier = Modifier.fillMaxWidth()) {
                        Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
                            Text(
                                text = selectedAnalysis!!.name,
                                fontFamily = SSPro,
                                fontSize = 20.sp,
                                fontWeight = FontWeight.SemiBold,
                                modifier = Modifier.fillMaxWidth(0.7f)
                            )
                            Box(
                                modifier = Modifier
                                    .size(24.dp)
                                    .clip(CircleShape)
                                    .background(inputBG)
                                    .clickable {
                                        scope.launch {
                                            sheetState.hide()
                                        }
                                    }
                            ) {
                                Icon(
                                    painter = painterResource(id = R.drawable.ic_close),
                                    contentDescription = "",
                                    modifier = Modifier
                                        .align(Alignment.Center),
                                    tint = descriptionColor
                                )
                            }
                        }
                        Spacer(modifier = Modifier.height(20.dp))
                        Column(modifier = Modifier.fillMaxWidth()) {
                            Text(
                                text = "Описание",
                                fontSize = 15.sp,
                                color = captionColor,
                                fontWeight = FontWeight.SemiBold
                            )
                            Spacer(modifier = Modifier.height(8.dp))
                            Text(
                                text = selectedAnalysis!!.description,
                                fontSize = 15.sp
                            )
                            Spacer(modifier = Modifier.height(16.dp))
                            Text(
                                text = "Подготовка",
                                fontSize = 15.sp,
                                color = captionColor,
                                fontWeight = FontWeight.SemiBold
                            )
                            Spacer(modifier = Modifier.height(8.dp))
                            Text(selectedAnalysis!!.preparation)
                        }
                    }
                    Column(modifier = Modifier.fillMaxWidth()) {
                        Spacer(modifier = Modifier.height(40.dp))
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                        ) {
                            Column(modifier = Modifier.fillMaxWidth(0.5f)) {
                                Text(
                                    text = "Результаты через:",
                                    fontSize = 14.sp,
                                    fontWeight = FontWeight.SemiBold,
                                    color = captionColor,
                                    fontFamily = SSPro
                                )
                                Spacer(modifier = Modifier.height(4.dp))
                                Text(
                                    text = selectedAnalysis!!.timerResult,
                                    fontSize = 16.sp,
                                    fontFamily = SSPro
                                )
                            }
                            Column(modifier = Modifier.fillMaxWidth()) {
                                Text(
                                    text = "Биоматериал:",
                                    fontSize = 14.sp,
                                    fontWeight = FontWeight.SemiBold,
                                    color = captionColor,
                                    fontFamily = SSPro
                                )
                                Spacer(modifier = Modifier.height(4.dp))
                                Text(
                                    text = selectedAnalysis!!.bio,
                                    fontSize = 16.sp,
                                    fontFamily = SSPro
                                )
                            }
                        }
                        Spacer(modifier = Modifier.height(19.dp))
                        var isInCart = false

                        for (item in cart) {
                            if (selectedAnalysis!!.id == item.id) {
                                isInCart = true
                            }
                        }
                        AppButton(
                            onClick = {
                                if (isInCart) {
                                    cart.removeIf { it.id == selectedAnalysis!!.id}
                                } else {
                                    cart.add(
                                        CartItem(
                                            selectedAnalysis!!.id,
                                            selectedAnalysis!!.name,
                                            selectedAnalysis!!.price,
                                            1,
                                        )
                                    )
                                }
                            },
                            label = if (isInCart) "Убрать" else  "Добавить за ${selectedAnalysis!!.price} ₽"
                        )
                    }
                }
            }
        },
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .drawBehind {
                        if (isSearchEnabled) drawLine(
                            iconsColor.copy(0.5f),
                            Offset(0f, size.height),
                            Offset(size.width, size.height),
                            1F
                        )
                    }
                    .padding(top = 24.dp, bottom = 24.dp, end = 20.dp, start = 20.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            ) {
                AppTextField(
                    value = searchText,
                    onValueChange = { searchText = it },
                    changeBorder = true,
                    modifier = Modifier
                        .fillMaxWidth( if (isSearchEnabled) 0.7f else 1f),
                    leadingIcon = {
                        Icon(
                            painter = painterResource(id = R.drawable.ic_search),
                            contentDescription = "",
                            tint = descriptionColor,
                            modifier = Modifier
                                .size(20.dp)
                        )
                    },
                    trailingIcon = {
                        if (isSearchEnabled) {
                            Box(
                                modifier = Modifier
                                    .size(24.dp)
                                    .clip(CircleShape)
                                    .clickable {
                                        searchText = ""
                                    }
                            ) {
                                Icon(
                                    painter = painterResource(id = R.drawable.ic_close),
                                    contentDescription = "",
                                    modifier = Modifier
                                        .align(Alignment.Center)
                                        .size(14.dp),
                                    tint = descriptionColor
                                )
                            }
                        }
                    },
                    placeholder = {
                        Text(text = "Искать анализы", fontSize = 16.sp, fontFamily = SSPro,)
                    },
                    interactionSource = interactionSource
                )
                if (isSearchEnabled) {
                    Spacer(modifier = Modifier.width(16.dp))
                    AppTextButton(
                        onClick = {
                            isSearchEnabled = false
                            searchText = ""
                        },
                        label = "Отменить",
                        style = TextStyle(
                            color = primary,
                            fontWeight = FontWeight.Normal
                        )
                    )
                }
            }
            AnimatedVisibility(visible = !isSearchEnabled) {
                Column() {
                    SwipeRefresh(
                        onRefresh = {
                            isLoading = true
                            viewModel.loadNews()
                            viewModel.loadCatalog()
                        },
                        state = rememberSwipeRefreshState(isRefreshing = isLoading)
                    ) {
                        Box(modifier = Modifier.fillMaxSize()) {
                            Column(
                                modifier = Modifier
                                    .padding(start = 20.dp)
                            ) {
                                AnimatedVisibility(visible = isNewsVisible) {
                                    Column(
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .verticalScroll(rememberScrollState())
                                    ) {
                                        Column(
                                            modifier = Modifier
                                                .fillMaxWidth()
                                                .padding(end = 20.dp)
                                        ) {
                                            Spacer(modifier = Modifier.height(8.dp))
                                            Text(
                                                text = "Акции и новости",
                                                fontSize = 17.sp,
                                                fontWeight = FontWeight.SemiBold,
                                                fontFamily = SSPro,
                                                color = captionColor
                                            )
                                            Spacer(modifier = Modifier.height(16.dp))
                                        }
                                        LazyRow {
                                            items(items = viewModel.news.distinct()) {
                                                NewsComponent(news = it)
                                            }
                                        }
                                        Spacer(modifier = Modifier.height(32.dp))
                                        Column(
                                            modifier = Modifier
                                                .fillMaxWidth()
                                                .padding(end = 20.dp)
                                        ) {
                                            Text(
                                                text = "Каталог анализов",
                                                fontSize = 17.sp,
                                                fontWeight = FontWeight.SemiBold,
                                                fontFamily = SSPro,
                                                color = captionColor
                                            )
                                            Spacer(modifier = Modifier.height(16.dp))
                                        }
                                    }
                                }
                                LazyRow(
                                    horizontalArrangement = Arrangement.spacedBy(16.dp)
                                ) {
                                    items(items = viewModel.categories.distinct()) {
                                        CategoryChip(
                                            name = it,
                                            selected = it == selectedCategory,
                                            onClick = { selectedCategory = it })
                                    }
                                }
                                Column(
                                    modifier = Modifier
                                        .padding(end = 20.dp, top = 8.dp)
                                        .verticalScroll(scrollState)
                                ) {
                                    for (analysis in viewModel.catalog.filter { it.category.lowercase() == selectedCategory.lowercase() }.distinct()) {
                                        var isInCart = false

                                        for (item in cart) {
                                            if (analysis.id == item.id) {
                                                isInCart = true
                                            }
                                        }

                                        CatalogCard(
                                            analysis = analysis,
                                            onButtonClick = { item ->
                                                if (!isInCart) {
                                                    cart.add(
                                                        CartItem(
                                                            item.id,
                                                            item.name,
                                                            item.price,
                                                            1
                                                        )
                                                    )
                                                } else {
                                                    cart.removeIf { it.id == item.id}
                                                }

                                                CartService().saveCart(sharedPreferences, cart)
                                            },
                                            isInCart = isInCart,
                                            onCardClick = {
                                                selectedAnalysis = analysis
                                                scope.launch {
                                                    sheetState.show()
                                                }
                                            }
                                        )
                                    }
                                    Spacer(modifier = Modifier.height(120.dp))
                                }
                            }
                            if (cart.isNotEmpty()) {
                                var sumPrice = 0

                                for (item in cart) {
                                    sumPrice += item.price.toInt()
                                }

                                Column(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .background(Color.White)
                                        .drawBehind {
                                            drawLine(
                                                iconsColor.copy(0.2f),
                                                Offset(0f, 0f),
                                                Offset(size.width, 0f),
                                                DefaultStrokeLineMiter
                                            )
                                        }
                                        .padding(20.dp, 24.dp)
                                        .align(Alignment.BottomCenter)
                                ) {
                                    Box(
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .clip(MaterialTheme.shapes.medium)
                                            .background(primary)
                                            .clickable {
                                                val intent = Intent(mContext, CartActivity::class.java)
                                                mContext.startActivity(intent)
                                            }
                                    ) {
                                        Row(
                                            Modifier
                                                .fillMaxWidth()
                                                .padding(16.dp),
                                            horizontalArrangement = Arrangement.SpaceBetween,
                                            verticalAlignment = Alignment.CenterVertically
                                        ) {
                                            Row(verticalAlignment = Alignment.CenterVertically) {
                                                Icon(
                                                    painter = painterResource(id = R.drawable.ic_cart),
                                                    contentDescription = "",
                                                    tint = Color.White,
                                                    modifier = Modifier.size(20.dp)
                                                )
                                                Spacer(modifier = Modifier.width(16.dp))
                                                Text(
                                                    text = "В корзину",
                                                    fontWeight = FontWeight.SemiBold,
                                                    fontSize = 17.sp,
                                                    color = Color.White
                                                )
                                            }
                                            Text(
                                                text = "$sumPrice ₽",
                                                fontWeight = FontWeight.SemiBold,
                                                fontSize = 17.sp,
                                                color = Color.White
                                            )
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            AnimatedVisibility(visible = isSearchEnabled) {
                Column(modifier = Modifier
                    .fillMaxSize()
                    .background(Color.White)) {
                    LazyColumn() {
                        items(items = viewModel.catalog.filter { it.name.lowercase().contains(searchText) && searchText.length > 2}.distinct()) {
                            SearchComponent(
                                analysis = it,
                                searchText = searchText
                            )
                        }
                    }
                }
            }
        }
    }

    if (isLoading) {
        LoadingDialog()
    }

    if (isErrorVisible) {
        AlertDialog(
            onDismissRequest = { isErrorVisible = false },
            title = {
                Text(text = "Ошибка", fontSize = 18.sp, fontWeight = FontWeight.SemiBold)
            },
            text = {
                Text(text = errorMessage!!, fontSize = 16.sp)
            },
            buttons = {
                AppTextButton(
                    onClick = {
                        isErrorVisible = false
                    },
                    label = "Ок"
                )
            }
        )
    }
}
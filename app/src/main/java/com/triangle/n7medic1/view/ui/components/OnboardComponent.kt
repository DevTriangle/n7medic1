package com.triangle.n7medic1.ui.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.triangle.n7medic1.ui.theme.*

/*
Описание: Компонент приветственного экрана
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
@Composable
fun OnboardComponent(
    info: Map<String, Any>,
    currentIndex: Int
) {
    Column(
        Modifier
            .widthIn(max = 400.dp)
            .fillMaxSize()
            .padding(horizontal = 20.dp, vertical = 60.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        Column(
            modifier = Modifier
                .widthIn(max = 400.dp)
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Column(
                modifier = Modifier
                    .height(120.dp)
                    .width(200.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = info["title"].toString(),
                    style = TextStyle(
                        fontSize = 20.sp,
                        fontWeight = FontWeight.Bold,
                        fontFamily = Lato,
                        textAlign = TextAlign.Center,
                        color = onboardTitle
                    )
                )
                Spacer(modifier = Modifier.height(29.dp))
                Text(
                    text = info["desc"].toString(),
                    style = TextStyle(
                        fontSize = 14.sp,
                        fontFamily = SSPro,
                        textAlign = TextAlign.Center,
                        color = captionColor
                    )
                )
            }
            Spacer(modifier = Modifier.height(60.dp))
            DotsIndicator(currentIndex, 3)
        }
        Image(
            painter = info["image"] as Painter,
            contentDescription = "",
            modifier = Modifier
                .height(240.dp),
            contentScale = ContentScale.FillHeight
        )
    }
}

/*
Описание: Индикатор номера экрана
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
@Composable
private fun DotsIndicator(
    currentIndex: Int,
    count: Int
) {
    Row() {
        for (i in 0 until count) {
            Box(
                modifier = Modifier
                    .padding(4.dp)
                    .size(14.dp)
                    .clip(CircleShape)
                    .border(0.65.dp, primaryVariant, CircleShape)
                    .background(if (currentIndex == i) primaryVariant else Color.White)
            )
        }
    }
}
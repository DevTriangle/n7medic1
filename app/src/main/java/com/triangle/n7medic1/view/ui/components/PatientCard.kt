package com.triangle.n7medic1.view.ui.components

import android.widget.Space
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.triangle.n7medic1.R
import com.triangle.n7medic1.model.CartItem
import com.triangle.n7medic1.model.User
import com.triangle.n7medic1.ui.theme.*
import kotlinx.coroutines.launch

/*
Описание: Карточка выбора пациента
Дата создания: 10.04.2023
Автор: Хасанов Альберт
 */
@Composable
fun PatientCard(
    user: User,
    selected: Boolean,
    onSelect: () -> Unit
) {
    Row(modifier = Modifier
        .fillMaxWidth()
        .clip(MaterialTheme.shapes.medium)
        .background(if (selected) primary else inputBG)
        .clickable {
            onSelect()
        }
        .padding(12.dp)
    ) {
        Image(
            painter = painterResource(id = if (user.gender == "Мужской") R.drawable.male else R.drawable.female),
            contentDescription = "",
            modifier = Modifier.size(24.dp)
        )
        Spacer(modifier = Modifier.width(12.dp))
        Text(
            text = "${user.lastName} ${user.firstName}",
            fontSize = 16.sp,
            color = if (selected) Color.White else Color.Black
        )
    }
}

/*
Описание: Карточка пациента при множетсенном выборе
Дата создания: 10.04.2023
Автор: Хасанов Альберт
 */
@Composable
fun MultiplePatientCard(
    user: User,
    cart: MutableList<CartItem>,
    onCartChange: (User, MutableList<CartItem>) -> Unit,
    onChangeClick: (User) -> Unit,
    onRemoveClick: (User) -> Unit
) {
    val tCart: MutableList<CartItem> = remember { mutableStateListOf() }
    LaunchedEffect(Unit) {
        tCart.addAll(cart)
    }
    
    Box(
        modifier = Modifier
            .border(1.dp, inputStroke, MaterialTheme.shapes.medium)
    ) {
        Column(modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp, 24.dp, 16.dp, 8.dp)) {
            Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
                AppTextField(
                    value = "${user.lastName} ${user.firstName}",
                    onValueChange = {},
                    changeBorder = false,
                    readOnly = true,
                    trailingIcon = {
                        IconButton(onClick = {
                            onChangeClick(user)
                        }) {
                            Icon(
                                painter = painterResource(id = R.drawable.ic_dropdown),
                                contentDescription = "",
                                tint = descriptionColor,
                            )
                        }
                    },
                    leadingIcon = {
                        Image(
                            painter = painterResource(id = if (user.gender == "Мужской") R.drawable.male else R.drawable.female),
                            contentDescription = "",
                            modifier = Modifier.size(24.dp)
                        )
                    },
                    modifier = Modifier.fillMaxWidth(0.7f)
                )
                IconButton(
                    onClick = { onRemoveClick(user) },
                    modifier = Modifier.size(24.dp)
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_close),
                        contentDescription = "",
                        tint = descriptionColor,
                        modifier = Modifier.size(14.dp)
                    )
                }
            }
            Spacer(modifier = Modifier.height(24.dp))
            for (c in cart) {
                var isChecked by rememberSaveable { mutableStateOf(user.cart.contains(c)) }
                
                Row(
                    modifier = Modifier.fillMaxWidth().padding(bottom = 8.dp),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Row(Modifier.fillMaxWidth(0.7f), verticalAlignment = Alignment.CenterVertically) {
                        Checkbox(
                            checked = isChecked,
                            onCheckedChange = {
                                isChecked = it

                                if (!it) tCart.remove(c)
                                else tCart.add(c)

                                onCartChange(user, tCart)
                            },
                            colors = CheckboxDefaults.colors(
                                checkmarkColor = Color.White,
                                uncheckedColor = inputBG,
                                checkedColor = primary
                            )
                        )
                        Spacer(modifier = Modifier.width(16.dp))
                        Text(
                            text = c.name,
                            fontSize = 12.sp,
                            color = if (isChecked) Color.Black else captionColor
                        )
                    }
                    Text(
                        text = "${c.price} ₽",
                        fontSize = 15.sp,
                        color = if (isChecked) Color.Black else captionColor
                    )
                }
            }
        }
    }
}
package com.triangle.n7medic1.view

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import com.triangle.n7medic1.common.EncryptDataSaver
import com.triangle.n7medic1.ui.theme.N7medic1Theme
import kotlinx.coroutines.launch

/*
Описание: Активити Launch-экрана
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val launchScreen = installSplashScreen()
        launchScreen.setKeepOnScreenCondition {true}

        setContent {
            N7medic1Theme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    ScreenContent()
                }
            }
        }
    }

    /*
    Описание: Содержание Launch-экрана
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    @SuppressLint("CoroutineCreationDuringComposition")
    @Composable
    fun ScreenContent() {
        val mContext = LocalContext.current
        val sharedPreferences = this.getSharedPreferences("shared", MODE_PRIVATE)
        val scope = rememberCoroutineScope()

        val isFirstLaunch = sharedPreferences.getBoolean("isFirstLaunch", true)
        val password = EncryptDataSaver(application).getPassword()

        val token = EncryptDataSaver(application).getToken()

        scope.launch {
            if (isFirstLaunch) {
                val intent = Intent(mContext, OnboardActivity::class.java)
                startActivity(intent)
            } else {
                if (token != "") {
                    if (password.isEmpty()) {
                        val intent = Intent(mContext, PasswordActivity::class.java)
                        startActivity(intent)
                    } else {
                        val intent = Intent(mContext, HomeActivity::class.java)
                        startActivity(intent)
                    }
                } else {
                    val intent = Intent(mContext, LoginActivity::class.java)
                    startActivity(intent)
                }
            }
        }
    }
}
package com.triangle.n7medic1.view.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.triangle.n7medic1.ui.theme.SSPro
import com.triangle.n7medic1.ui.theme.descriptionColor
import com.triangle.n7medic1.ui.theme.inputBG
import com.triangle.n7medic1.ui.theme.primary
import java.util.Locale.Category

/*
Описание: Компонент категории
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
@Composable
fun CategoryChip(
    name: String,
    selected: Boolean,
    onClick: () -> Unit
) {
    Box(
        modifier = Modifier
            .clip(MaterialTheme.shapes.medium)
            .background(if (selected) primary else inputBG)
            .clickable { onClick() }
    ) {
        Text(
            modifier = Modifier.padding(20.dp, 14.dp),
            text = name,
            fontSize = 15.sp,
            fontFamily = SSPro,
            fontWeight = FontWeight.SemiBold,
            textAlign = TextAlign.Center,
            color = if (selected) Color.White else descriptionColor
        )
    }
}
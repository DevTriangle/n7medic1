package com.triangle.n7medic1.view.ui.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.triangle.n7medic1.model.CartItem
import com.triangle.n7medic1.ui.components.AppButton
import com.triangle.n7medic1.ui.theme.*
import com.triangle.n7medic1.R
import kotlinx.coroutines.launch

/*
Описание: Компонент товара в корзине
Дата создания: 06.04.2023
Автор: Хасанов Альберт
 */
@Composable
fun CartComponent(
    modifier: Modifier = Modifier,
    cartItem: CartItem,
    onRemoveClick: (CartItem) -> Unit,
    onAddClick: (CartItem) -> Unit,
    onMinusClick: (CartItem) -> Unit,
) {
    Box(
        modifier = modifier
            .padding(top = 8.dp, bottom = 8.dp)
            .shadow(10.dp, shape = MaterialTheme.shapes.large, spotColor = Color.Black.copy(0.2f))
            .clip(MaterialTheme.shapes.large)
            .background(Color.White)
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp),
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Row(
                Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = cartItem.name,
                    fontSize = 16.sp,
                    fontFamily = SSPro,
                    modifier = Modifier
                        .fillMaxWidth(0.7f)
                )
                Box(
                    modifier = Modifier
                        .size(24.dp)
                        .clip(CircleShape)
                        .clickable {
                            onRemoveClick(cartItem)
                        }
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_close),
                        contentDescription = "",
                        modifier = Modifier
                            .align(Alignment.Center)
                            .size(14.dp),
                        tint = descriptionColor
                    )
                }
            }
            Spacer(modifier = Modifier.height(38.dp))
            Row(
                Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = (cartItem.price.toInt() * cartItem.count).toString() + " ₽",
                    fontFamily = SSPro,
                    fontSize = 16.sp
                )
                Row(verticalAlignment = Alignment.CenterVertically) {
                    Text(
                        text = "${cartItem.count} пациент",
                        fontSize = 15.sp,
                        fontFamily = SSPro
                    )
                    Spacer(modifier = Modifier.width(16.dp))
                    AddRemoveButtons(
                        onAddClick = {
                            onAddClick(cartItem)
                        },
                        onRemoveClick = {
                            onMinusClick(cartItem)
                        },
                        count = cartItem.count
                    )
                }
            }
        }
    }
}

/*
Описание: Компонент кнопок уменьшения и увеличения кол-ва товаров
Дата создания: 06.04.2023
Автор: Хасанов Альберт
 */
@Composable
private fun AddRemoveButtons(
    onAddClick: () -> Unit,
    onRemoveClick: () -> Unit,
    count: Int
) {
    Box(
        modifier = Modifier
            .width(64.dp)
            .height(32.dp)
            .clip(MaterialTheme.shapes.small)
            .background(inputBG)
    ) {
        Row(modifier = Modifier
            .fillMaxWidth()
            .padding(6.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            IconButton(onClick = { onRemoveClick() }, modifier = Modifier.size(20.dp), enabled = count > 1) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_minus),
                    contentDescription = "",
                    tint = if (count == 1) iconsColor else captionColor
                )
            }
            Box(modifier = Modifier
                .width(2.dp)
                .fillMaxHeight()
                .background(inputStroke))
            IconButton(onClick = { onAddClick() }, modifier = Modifier.size(20.dp)) {
                Icon(
                    painter = painterResource(id = R.drawable.ci_plus),
                    contentDescription = "",
                    tint = captionColor
                )
            }
        }
    }
}
package com.triangle.n7medic1.view.ui.components

import android.graphics.LinearGradient
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.node.modifierElementOf
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.glide.GlideImage
import com.triangle.n7medic1.model.News


/*
Описание: Компонент новостей
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
@Composable
fun NewsComponent(
    news: News
) {
    Box(
        modifier = Modifier
            .width(270.dp).height(152.dp)
            .padding(end = 16.dp)
            .clip(MaterialTheme.shapes.large)
            .background(Brush.horizontalGradient(listOf(Color(0xFF76B3FF), Color(0xFFCDE3FF)))),
    ) {
        GlideImage(
            news.image,
            imageOptions = ImageOptions(
                alignment = Alignment.BottomEnd,
                contentScale = ContentScale.FillHeight
            ),
            requestOptions = {
                RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
            }
        )
        Column(
            modifier = Modifier
                .fillMaxWidth(0.6f)
                .fillMaxHeight()
                .padding(start = 16.dp, top = 16.dp, bottom = 12.dp),
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = news.name,
                fontSize = 18.sp,
                fontWeight = FontWeight.Bold,
                color = Color.White,
                lineHeight = 18.sp
            )
            Column(modifier = Modifier.fillMaxWidth()) {
                Text(
                    text = news.description,
                    fontSize = 12.sp,
                    color = Color.White,
                    lineHeight = 12.sp
                )
                Text(
                    text = news.price + " ₽",
                    fontSize = 18.sp,
                    fontWeight = FontWeight.Bold,
                    color = Color.White
                )
            }
        }
    }
}
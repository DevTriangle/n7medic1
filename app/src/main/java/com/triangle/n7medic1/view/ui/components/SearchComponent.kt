package com.triangle.n7medic1.view.ui.components

import android.widget.Space
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.vector.DefaultStrokeLineMiter
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.triangle.n7medic1.model.Analysis
import com.triangle.n7medic1.ui.theme.captionColor
import com.triangle.n7medic1.ui.theme.iconsColor
import com.triangle.n7medic1.ui.theme.primary

/*
Описание: Компонент товара в поиске
Дата создания: 06.04.2023
Автор: Хасанов Альберт
 */
@Composable
fun SearchComponent(
    analysis: Analysis,
    searchText: String
) {
    val text = analysis.name.split(searchText.lowercase())
    Box(modifier = Modifier.fillMaxWidth().drawBehind {
        drawLine(
            iconsColor.copy(0.5f),
            Offset(0f, size.height),
            Offset(size.width, size.height),
            0.9F
        )
    }) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(20.dp, 12.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = buildAnnotatedString {
                    for ((index, t) in text.withIndex()) {
                        append(t)

                        if (index != text.size - 1) {
                            withStyle(style = SpanStyle(color = primary)) {
                                append(searchText)
                            }
                        }
                    }
                },
                fontSize = 15.sp,
                modifier = Modifier
                    .fillMaxWidth(0.7f)
            )
            Column() {
                Text(
                    text = analysis.price + " ₽",
                    fontSize = 17.sp,
                    textAlign = TextAlign.Right
                )
                Spacer(modifier = Modifier.height(1.dp))
                Text(
                    text = analysis.timerResult,
                    fontSize = 14.sp,
                    textAlign = TextAlign.Right,
                    color = captionColor
                )
            }
        }
    }
}
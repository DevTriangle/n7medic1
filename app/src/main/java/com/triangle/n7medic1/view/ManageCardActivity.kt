package com.triangle.n7medic1.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.lifecycle.ViewModelProvider
import com.triangle.n7medic1.R
import com.triangle.n7medic1.common.DataSaver
import com.triangle.n7medic1.common.EncryptDataSaver
import com.triangle.n7medic1.model.User
import com.triangle.n7medic1.ui.components.AppButton
import com.triangle.n7medic1.ui.components.AppTextButton
import com.triangle.n7medic1.ui.components.PasswordButton
import com.triangle.n7medic1.ui.theme.*
import com.triangle.n7medic1.view.ui.components.AppTextField
import com.triangle.n7medic1.view.ui.components.LoadingDialog
import com.triangle.n7medic1.viewmodel.LoginViewModel
import com.triangle.n7medic1.viewmodel.UserViewModel

/*
Описание: Активити экрана создания карты пацеинта
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
class ManageCardActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            N7medic1Theme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background,
                ) {
                    ScreenContent()
                }
            }
        }
    }

    /*
    Описание: Содержание экрана создания карты пацеинта
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    @OptIn(ExperimentalMaterialApi::class)
    @Composable
    fun ScreenContent() {
        val mContext = LocalContext.current
        val sharedPreferences = this.getSharedPreferences("shared", MODE_PRIVATE)
        val viewModel = ViewModelProvider(this)[UserViewModel::class.java]

        var firstName by rememberSaveable { mutableStateOf("") }
        var lastName by rememberSaveable { mutableStateOf("") }
        var patronymic by rememberSaveable { mutableStateOf("") }
        var birthday by rememberSaveable { mutableStateOf("") }
        var gender by rememberSaveable { mutableStateOf("") }

        var isExpanded by rememberSaveable { mutableStateOf(false) }

        var isLoading by rememberSaveable { mutableStateOf(false) }
        var isErrorVisible by rememberSaveable { mutableStateOf(false) }

        val newUser by viewModel.newUser.observeAsState()
        LaunchedEffect(newUser) {
            if (newUser != null) {
                isLoading = false

                var userList: MutableList<User> = DataSaver().loadUsers(sharedPreferences)
                userList.add(newUser!!)
                DataSaver().saveUsers(sharedPreferences, userList)

                val intent = Intent(mContext, HomeActivity::class.java)
                startActivity(intent)
            }
        }

        val errorMessage by viewModel.message.observeAsState()
        LaunchedEffect(errorMessage) {
            if (errorMessage != null) {
                isLoading = false
                isErrorVisible = true
            }
        }

        Scaffold(topBar = {
            Column(modifier = Modifier
                .fillMaxWidth()
                .verticalScroll(rememberScrollState()),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Row(
                    Modifier
                        .widthIn(max = 400.dp)
                        .fillMaxWidth()
                        .padding(start = 20.dp, top = 32.dp, end = 20.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(
                        text = "Создание карты пациента",
                        fontSize = 24.sp,
                        fontWeight = FontWeight.Bold,
                        fontFamily = SSPro,
                        softWrap = true,
                        modifier = Modifier.fillMaxWidth(0.65f)
                    )
                    AppTextButton(
                        onClick = {
                            val intent = Intent(mContext, HomeActivity::class.java)
                            startActivity(intent)
                        },
                        label = "Пропустить",
                        style = TextStyle(
                            fontSize = 15.sp,
                            fontFamily = SSPro,
                            color = primary
                        ),
                        modifier = Modifier
                            .padding(start = 16.dp)
                            .fillMaxWidth()
                    )
                }
            }
        }) {
            Box(modifier = Modifier.padding(it)) {
                Column(modifier = Modifier
                    .fillMaxSize()
                    .verticalScroll(rememberScrollState()),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Column(
                        Modifier
                            .widthIn(max = 400.dp)
                            .padding(horizontal = 20.dp, vertical = 16.dp)
                            .fillMaxSize()) {
                        Text(
                            text = "Без карты пациента вы не сможете заказать анализы.",
                            color = captionColor,
                            fontSize = 14.sp,
                            fontFamily = SSPro
                        )
                        Spacer(modifier = Modifier.height(8.dp))
                        Text(
                            text = "В картах пациентов будут храниться результаты анализов вас и ваших близких.",
                            color = captionColor,
                            fontSize = 14.sp,
                            fontFamily = SSPro
                        )
                        Spacer(modifier = Modifier.height(32.dp))
                        AppTextField(
                            value = firstName,
                            onValueChange = {
                                firstName = it
                            },
                            changeBorder = true,
                            placeholder = {
                                Text(
                                    text = "Имя",
                                    fontFamily = SSPro,
                                    fontSize = 15.sp,
                                    color = captionColor
                                )
                            },
                            modifier = Modifier.fillMaxWidth()
                        )
                        Spacer(modifier = Modifier.height(24.dp))
                        AppTextField(
                            value = patronymic,
                            onValueChange = {
                                patronymic = it
                            },
                            placeholder = {
                                Text(
                                    text = "Отчество",
                                    fontFamily = SSPro,
                                    fontSize = 15.sp,
                                    color = captionColor
                                )
                            },
                            changeBorder = true,
                            modifier = Modifier.fillMaxWidth()
                        )
                        Spacer(modifier = Modifier.height(24.dp))
                        AppTextField(
                            value = lastName,
                            onValueChange = {
                                lastName = it
                            },
                            placeholder = {
                                Text(
                                    text = "Фамилия",
                                    fontFamily = SSPro,
                                    fontSize = 15.sp,
                                    color = captionColor
                                )
                            },
                            changeBorder = true,
                            modifier = Modifier.fillMaxWidth()
                        )
                        Spacer(modifier = Modifier.height(24.dp))
                        AppTextField(
                            value = birthday,
                            onValueChange = {
                                birthday = it
                            },
                            placeholder = {
                                Text(
                                    text = "Дата рождения",
                                    fontFamily = SSPro,
                                    fontSize = 15.sp,
                                    color = captionColor
                                )
                            },
                            changeBorder = true,
                            modifier = Modifier.fillMaxWidth()
                        )
                        Spacer(modifier = Modifier.height(24.dp))
                        ExposedDropdownMenuBox(
                            expanded = isExpanded,
                            onExpandedChange = {
                                isExpanded = it
                            }
                        ) {
                            AppTextField(
                                value = gender,
                                onValueChange = {
                                    gender = it
                                },
                                placeholder = {
                                    Text(
                                        text = "Пол",
                                        fontFamily = SSPro,
                                        fontSize = 15.sp,
                                        color = captionColor
                                    )
                                },
                                readOnly = true,
                                trailingIcon = {
                                     IconButton(onClick = { isExpanded = true }) {
                                         Icon(
                                             painter = painterResource(id = R.drawable.ic_dropdown),
                                             contentDescription = "",
                                             tint = descriptionColor,
                                         )
                                     }
                                },
                                changeBorder = false,
                                modifier = Modifier.fillMaxWidth()
                            )
                            DropdownMenu(expanded = isExpanded, onDismissRequest = { isExpanded = false }) {
                                DropdownMenuItem(onClick = {
                                    gender = "Мужской"
                                    isExpanded = false
                                }) {
                                    Text(text = "Мужской")
                                }
                                DropdownMenuItem(onClick = {
                                    gender = "Женский"
                                    isExpanded = false
                                }) {
                                    Text(text = "Женский")
                                }
                            }
                        }
                        Spacer(modifier = Modifier.height(48.dp))
                        AppButton(
                            onClick = {
                                isLoading = true
                                viewModel.createProfile(
                                    firstName,
                                    patronymic,
                                    lastName,
                                    birthday,
                                    gender,
                                    EncryptDataSaver(application).getToken()
                                )
                            },
                            label = "Создать",
                            enabled = firstName.isNotBlank() && patronymic.isNotBlank() && lastName.isNotBlank() && birthday.isNotBlank() && gender.isNotBlank()
                        )
                    }
                }
            }
        }

        if (isLoading) {
            LoadingDialog()
        }

        if (isErrorVisible) {
            AlertDialog(
                onDismissRequest = { isErrorVisible = false },
                title = {
                    Text(text = "Ошибка", fontSize = 18.sp, fontWeight = FontWeight.SemiBold)
                },
                text = {
                    Text(text = errorMessage!!, fontSize = 16.sp)
                },
                buttons = {
                    AppTextButton(
                        onClick = {
                            isErrorVisible = false
                        },
                        label = "Ок"
                    )
                }
            )
        }
    }
}
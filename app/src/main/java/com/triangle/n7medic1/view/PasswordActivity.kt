package com.triangle.n7medic1.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.substring
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.lifecycle.ViewModelProvider
import com.triangle.n7medic1.R
import com.triangle.n7medic1.common.DataSaver
import com.triangle.n7medic1.common.EncryptDataSaver
import com.triangle.n7medic1.ui.components.AppTextButton
import com.triangle.n7medic1.ui.components.PasswordButton
import com.triangle.n7medic1.ui.theme.*
import com.triangle.n7medic1.viewmodel.LoginViewModel

/*
Описание: Активити экрана создания пароля
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
class PasswordActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            N7medic1Theme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background,
                ) {
                    ScreenContent()
                }
            }
        }
    }

    /*
    Описание: Содержание экрана создания пароля
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    @Composable
    fun ScreenContent() {
        val mContext = LocalContext.current

        var password by rememberSaveable { mutableStateOf("") }

        Scaffold(topBar = {
            Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.End) {
                AppTextButton(
                    onClick = {
                        val intent = Intent(mContext, ManageCardActivity::class.java)
                        startActivity(intent)
                    },
                    label = "Пропустить",
                    style = TextStyle(
                        fontSize = 15.sp,
                        fontFamily = SSPro,
                        color = primary
                    ),
                    modifier = Modifier.padding(end = 20.dp)
                )
            }
        }) {
            Box(modifier = Modifier.padding(it)) {
                Column(
                    Modifier
                        .fillMaxSize()
                        .padding(top = 60.dp, bottom = 80.dp, start = 44.dp, end = 44.dp)
                        .verticalScroll(rememberScrollState()),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Column(
                        Modifier
                            .widthIn(max = 400.dp)
                            .fillMaxWidth(),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Text(
                            text = "Создайте пароль",
                            fontSize = 24.sp,
                            fontWeight = FontWeight.Bold,
                            fontFamily = SSPro,
                            textAlign = TextAlign.Center
                        )
                        Spacer(modifier = Modifier.height(16.dp))
                        Text(
                            text = "Для защиты ваших персональных данных",
                            color = captionColor,
                            fontSize = 15.sp,
                            fontFamily = SSPro,
                            textAlign = TextAlign.Center
                        )
                        Spacer(modifier = Modifier.height(56.dp))
                        PasswordCountIndicator(currentCount = password.length, 4)
                        Spacer(modifier = Modifier.height(60.dp))
                        LazyVerticalGrid(
                            columns = GridCells.Fixed(3),
                            horizontalArrangement = Arrangement.spacedBy(24.dp),
                            verticalArrangement = Arrangement.spacedBy(24.dp),
                            modifier = Modifier
                                .height(392.dp)
                                .width(288.dp),
                            userScrollEnabled = false,
                            content = {
                                items(count = 12) { i ->
                                    if (i < 9) {
                                        PasswordButton(
                                            onClick = {
                                                if (password.length == 3) {
                                                    password += "${i+1}"

                                                    EncryptDataSaver(application).savePassword(password)

                                                    val intent = Intent(mContext, ManageCardActivity::class.java)
                                                    startActivity(intent)
                                                } else {
                                                    password += "${i+1}"
                                                }

                                                Log.d("TAG", "ScreenContent: $password")
                                            },
                                            label = i+1
                                        )
                                    } else if (i == 10) {
                                        PasswordButton(
                                            onClick = {
                                                if (password.length == 3) {
                                                    password += "0"

                                                    EncryptDataSaver(application).savePassword(password)

                                                    val intent = Intent(mContext, ManageCardActivity::class.java)
                                                    startActivity(intent)
                                                } else {
                                                    password += "0"
                                                }

                                                Log.d("TAG", "ScreenContent: $password")
                                            },
                                            label = 0
                                        )
                                    } else if (i == 11) {
                                        Box(
                                            modifier = Modifier
                                                .size(80.dp)
                                                .clip(CircleShape)
                                                .clickable(
                                                    onClick = {
                                                        if (password.isNotEmpty()) {
                                                            password = password.substring(0, password.length - 1)
                                                        }
                                                    }
                                                )
                                        ) {
                                            Icon(
                                                painter = painterResource(id = R.drawable.del_icon),
                                                contentDescription = "",
                                                modifier = Modifier
                                                    .align(Alignment.Center)
                                                    .size(35.dp)
                                            )
                                        }
                                    }
                                }
                            }
                        )
                    }
                }
            }
        }
    }

    /*
    Описание: Индикатор кол-ва введенных символов
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    @Composable
    private fun PasswordCountIndicator(
        currentCount: Int,
        count: Int
    ) {
        Row(horizontalArrangement = Arrangement.Center) {
            for (i in 1 .. count) {
                Box(
                    modifier = Modifier
                        .padding(6.dp)
                        .size(16.dp)
                        .clip(CircleShape)
                        .border(1.dp, primary, CircleShape)
                        .background(if (i <= currentCount) primary else Color.White)
                )
            }
        }
    }
}
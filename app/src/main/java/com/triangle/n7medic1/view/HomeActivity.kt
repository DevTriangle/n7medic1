package com.triangle.n7medic1.view

import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.DefaultStrokeLineMiter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.triangle.n7medic1.ui.theme.N7medic1Theme
import com.triangle.n7medic1.R
import com.triangle.n7medic1.model.User
import com.triangle.n7medic1.ui.theme.SSPro
import com.triangle.n7medic1.ui.theme.iconsColor
import com.triangle.n7medic1.ui.theme.primary
import com.triangle.n7medic1.view.screens.AnalyzesScreen
import com.triangle.n7medic1.view.screens.ProfileScreen
import com.triangle.n7medic1.viewmodel.HomeViewModel
import com.triangle.n7medic1.viewmodel.UserViewModel
import kotlin.math.log

/*
Описание: Активити главной страницы
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
class HomeActivity : ComponentActivity() {

    /*
    Описание: ResultLauncher для выбора изображений
    Дата создания: 06.04.2023
    Автор: Хасанов Альберт
     */
    private val imageResultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == RESULT_OK) {
            val userViewModel = ViewModelProvider(this)[UserViewModel::class.java]

            val data = result.data
            val image = data?.extras?.get("data")

            userViewModel.image.value = image as Bitmap
            userViewModel.isVideo.value = false
        }
    }

    /*
    Описание: ResultLauncher для выбора видео
    Дата создания: 06.04.2023
    Автор: Хасанов Альберт
     */
    val videoResultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == RESULT_OK) {
            val userViewModel = ViewModelProvider(this)[UserViewModel::class.java]

            val data = result.data
            val video = data?.data

            userViewModel.video.value = video
            userViewModel.isVideo.value = true
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val navController = rememberNavController()
            val navBackStackEntry = navController.currentBackStackEntryAsState()

            N7medic1Theme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Scaffold(
                        bottomBar = {
                            BottomNavigation(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .height(75.dp)
                                    .drawBehind {
                                        drawLine(
                                            iconsColor.copy(0.5f),
                                            Offset(0f, 0f),
                                            Offset(size.width, 0f),
                                            DefaultStrokeLineMiter
                                        )
                                    },
                                backgroundColor = Color.White,
                                elevation = 0.dp,
                                contentColor = Color(0xFFB8C1CC)
                            ) {
                                Row(
                                    Modifier
                                        .fillMaxWidth(),
                                    verticalAlignment = Alignment.CenterVertically,
                                    horizontalArrangement = Arrangement.SpaceBetween
                                ) {
                                    BottomNavigationItem(
                                        selected = navBackStackEntry.value?.destination?.route == "analyzes",
                                        onClick = {
                                            navController.navigate("analyzes")
                                        },
                                        selectedContentColor = primary,
                                        unselectedContentColor = iconsColor,
                                        icon = {
                                            Icon(
                                                painter = painterResource(id = R.drawable.analyzes),
                                                contentDescription = "",
                                            )
                                        },
                                        label = {
                                            Text(text = "Анализы", fontSize = 12.sp, fontFamily = SSPro)
                                        },
                                        modifier = Modifier.padding(vertical = 8.dp)
                                    )
                                    BottomNavigationItem(
                                        selected = navBackStackEntry.value?.destination?.route == "results",
                                        onClick = {
                                            navController.navigate("results")
                                        },
                                        selectedContentColor = primary,
                                        unselectedContentColor = iconsColor,
                                        icon = {
                                            Icon(
                                                painter = painterResource(id = R.drawable.results),
                                                contentDescription = "",
                                            )
                                        },
                                        label = {
                                            Text(text = "Результаты", fontSize = 12.sp, fontFamily = SSPro)
                                        },
                                        modifier = Modifier.padding(vertical = 8.dp)
                                    )
                                    BottomNavigationItem(
                                        selected = navBackStackEntry.value?.destination?.route == "support",
                                        onClick = {
                                            navController.navigate("support")
                                        },
                                        selectedContentColor = primary,
                                        unselectedContentColor = iconsColor,
                                        icon = {
                                            Icon(
                                                painter = painterResource(id = R.drawable.support),
                                                contentDescription = "",
                                            )
                                        },
                                        label = {
                                            Text(text = "Поддержка", fontSize = 12.sp, fontFamily = SSPro)
                                        },
                                        modifier = Modifier.padding(vertical = 8.dp)
                                    )
                                    BottomNavigationItem(
                                        selected = navBackStackEntry.value?.destination?.route == "profile",
                                        onClick = {
                                            navController.navigate("profile")
                                        },
                                        selectedContentColor = primary,
                                        unselectedContentColor = iconsColor,
                                        icon = {
                                            Icon(
                                                painter = painterResource(id = R.drawable.profile),
                                                contentDescription = "",
                                            )
                                        },
                                        label = {
                                            Text(text = "Профиль", fontSize = 12.sp, fontFamily = SSPro)
                                        },
                                        modifier = Modifier.padding(vertical = 8.dp)
                                    )
                                }
                            }
                        }
                    ) {
                        Box(modifier = Modifier.padding(it)) {
                            Navigation(navController = navController)
                        }
                    }
                }
            }
        }
    }

    /*
    Описание: Навигация через нижнее меню
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    @Composable
    fun Navigation(navController: NavHostController) {
        val homeViewModel = ViewModelProvider(this)[HomeViewModel::class.java]
        val userViewModel = ViewModelProvider(this)[UserViewModel::class.java]

        NavHost(navController = navController, startDestination = "analyzes") {
            composable("analyzes") {
                AnalyzesScreen(homeViewModel)
            }
            composable("results") {
                Box(modifier = Modifier.fillMaxSize()) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_app_foreground),
                        contentDescription = "",
                        modifier = Modifier
                            .clip(CircleShape)
                            .align(Alignment.Center)
                    )
                }
            }
            composable("support") {
                Box(modifier = Modifier.fillMaxSize()) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_app_foreground),
                        contentDescription = "",
                        modifier = Modifier
                            .clip(CircleShape)
                            .align(Alignment.Center)
                    )
                }
            }
            composable("profile") {
                ProfileScreen(userViewModel, application, imageResultLauncher, videoResultLauncher)
            }
        }
    }
}
package com.triangle.n7medic1.view

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.lifecycle.ViewModelProvider
import com.triangle.n7medic1.R
import com.triangle.n7medic1.ui.components.AppButton
import com.triangle.n7medic1.ui.components.AppTextButton
import com.triangle.n7medic1.ui.theme.*
import com.triangle.n7medic1.view.ui.components.AppTextField
import com.triangle.n7medic1.view.ui.components.LoadingDialog
import com.triangle.n7medic1.viewmodel.LoginViewModel

/*
Описание: Активити экрана авторизации
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
class LoginActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            N7medic1Theme {
                Surface(
                    modifier = Modifier
                        .fillMaxSize()
                        .testTag("login"),
                    color = MaterialTheme.colors.background,
                ) {
                    ScreenContent()
                }
            }
        }
    }

    /*
    Описание: Содержание экрана авторизации
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    @Composable
    fun ScreenContent() {
        val mContext = LocalContext.current
        val viewModel = ViewModelProvider(this)[LoginViewModel::class.java]
        var email by rememberSaveable { mutableStateOf("") }

        var isLoading by rememberSaveable { mutableStateOf(false) }
        var isErrorVisible by rememberSaveable { mutableStateOf(false) }

        val isSuccess by viewModel.isSuccessSendCode.observeAsState()
        LaunchedEffect(isSuccess) {
            if (isSuccess == true) {
                isLoading = false

                val intent = Intent(mContext, CodeActivity::class.java)
                intent.putExtra("email", email)
                startActivity(intent)
            } else {
                isLoading = false
            }
        }

        val errorMessage by viewModel.message.observeAsState()
        LaunchedEffect(errorMessage) {
            if (errorMessage != null) {
                isLoading = false
                isErrorVisible = true
            }
        }

        Column(
            Modifier
                .fillMaxSize()
                .verticalScroll(rememberScrollState())
                .padding(horizontal = 20.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Column(modifier = Modifier
                .widthIn(max = 400.dp)
                .padding(top = 60.dp)
                .fillMaxWidth()) {
                Row(modifier = Modifier
                    .widthIn(max = 400.dp)
                    .fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_hello),
                        contentDescription = "",
                        modifier = Modifier
                            .size(32.dp)
                    )
                    Spacer(modifier = Modifier.widthIn(16.dp))
                    Text(
                        text = "Добро пожаловать!",
                        fontSize = 24.sp,
                        fontFamily = SSPro,
                        fontWeight = FontWeight.Bold
                    )
                }
                Spacer(modifier = Modifier.height(23.dp))
                Text(
                    text = "Войдите, чтобы пользоваться функциями приложения",
                    fontFamily = SSPro,
                    fontSize = 15.sp
                )
                Spacer(modifier = Modifier.height(64.dp))
                AppTextField(
                    value = email,
                    onValueChange = {email = it},
                    changeBorder = false,
                    modifier = Modifier.fillMaxWidth(),
                    label = {
                        Text(
                            text = "Вход по E-mail",
                            fontSize = 14.sp,
                            color = descriptionColor,
                            fontFamily = SSPro
                        )
                    },
                    placeholder = {
                        Text(
                            text = "example@mail.ru",
                            fontSize = 15.sp,
                            color = Color.Black.copy(0.5f),
                            fontFamily = SSPro
                        )
                    }
                )
                Spacer(modifier = Modifier.height(32.dp))
                AppButton(
                    onClick = {
                        if (Regex("^[a-z0-9]*@[a-z0-9]*\\.[a-z0-9]+").matches(email)) {
                            isLoading = true
                            viewModel.sendCode(email)
                        } else {
                            viewModel.message.value = null
                            viewModel.message.value = "Неверный формат E-Mail"
                        }
                              },
                    label = "Далее",
                    enabled = email.isNotBlank(),
                )
            }
            Column(
                Modifier
                    .widthIn(max = 400.dp)
                    .fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Spacer(modifier = Modifier.height(32.dp))
                Text(
                    text = "Или войдите с помощью",
                    fontFamily = SSPro,
                    fontSize = 15.sp,
                    color = captionColor
                )
                Spacer(modifier = Modifier.height(16.dp))
                AppButton(
                    onClick = {

                    },
                    label = "Войти с Яндекс",
                    border = BorderStroke(1.dp, inputStroke),
                    style = TextStyle(
                        fontWeight = FontWeight.Normal
                    ),
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = Color.White,
                        contentColor = Color.Black
                    )
                )
                Spacer(modifier = Modifier.height(55.dp))
            }
        }
        
        if (isLoading) {
            LoadingDialog()
        }
        
        if (isErrorVisible) {
            AlertDialog(
                onDismissRequest = { isErrorVisible = false },
                title = {
                    Text(text = "Ошибка", fontSize = 18.sp, fontWeight = FontWeight.SemiBold)
                },
                text = {
                    Text(text = errorMessage!!, fontSize = 16.sp)
                },
                buttons = {
                    AppTextButton(
                        onClick = {
                            isErrorVisible = false
                        },
                        label = "Ок"
                    )
                }
            )
        }
    }
}
package com.triangle.n7medic1.view.ui.components

import android.content.Context
import android.widget.Space
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.triangle.n7medic1.R
import com.triangle.n7medic1.common.DataSaver
import com.triangle.n7medic1.model.AppAddress
import com.triangle.n7medic1.ui.components.AppButton
import com.triangle.n7medic1.ui.theme.*
import kotlinx.coroutines.launch

/*
Описание: Экран выбора адреса
Дата создания: 10.04.2023
Автор: Хасанов Альберт
 */
@Composable
fun AddressBottomSheet(
    onAddressChange: (String) -> Unit,
    addr: String,
    tLat: String,
    tLon: String,
    tAlt: String,
) {
    val mContext = LocalContext.current
    val sharedPreferences = mContext.getSharedPreferences("shared", Context.MODE_PRIVATE)

    var address by rememberSaveable { mutableStateOf(addr) }
    var addressName by rememberSaveable { mutableStateOf("") }

    var lat by rememberSaveable { mutableStateOf(tLat) }
    var lon by rememberSaveable { mutableStateOf(tLon) }
    var alt by rememberSaveable { mutableStateOf(tAlt) }

    var flat by rememberSaveable { mutableStateOf("") }
    var entrance by rememberSaveable { mutableStateOf("") }
    var floor by rememberSaveable { mutableStateOf("") }

    var doorphone by rememberSaveable { mutableStateOf("") }

    var saveAddress by rememberSaveable { mutableStateOf(false) }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(20.dp, 24.dp)
    ) {
        Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
            Text(
                text = "Адрес сдачи анализов",
                fontFamily = SSPro,
                fontSize = 20.sp,
                fontWeight = FontWeight.SemiBold,
                modifier = Modifier.fillMaxWidth(0.7f)
            )
            Box(
                modifier = Modifier
                    .size(24.dp)
                    .clickable {}
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_map),
                    contentDescription = "",
                    modifier = Modifier
                        .align(Alignment.Center),
                    tint = iconsColor
                )
            }
        }
        Spacer(modifier = Modifier.height(8.dp))
        AppTextField(
            value = address,
            onValueChange = { address = it },
            changeBorder = false,
            label = {
                Text(
                    text = "Ваш адрес",
                    fontSize = 14.sp,
                    color = descriptionColor
                )
            },
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(16.dp))
        Row(modifier = Modifier.fillMaxWidth()) {
            AppTextField(
                value = lat,
                onValueChange = { lat = it },
                changeBorder = false,
                label = {
                    Text(
                        text = "Долгота",
                        fontSize = 14.sp,
                        color = descriptionColor
                    )
                },
                columnModifier = Modifier
                    .weight(2f)
                    .padding(end = 12.5.dp),
                modifier = Modifier.fillMaxWidth()
            )
            AppTextField(
                value = lon,
                onValueChange = { lon = it },
                changeBorder = false,
                label = {
                    Text(
                        text = "Широта",
                        fontSize = 14.sp,
                        color = descriptionColor
                    )
                },
                columnModifier = Modifier
                    .weight(2f)
                    .padding(end = 12.5.dp),
                modifier = Modifier.fillMaxWidth()
            )
            AppTextField(
                value = alt,
                onValueChange = { alt = it },
                changeBorder = false,
                label = {
                    Text(
                        text = "Высота",
                        fontSize = 14.sp,
                        color = descriptionColor
                    )
                },
                columnModifier = Modifier
                    .weight(1f),
                modifier = Modifier.fillMaxWidth()
            )
        }
        Spacer(modifier = Modifier.height(16.dp))
        Row(modifier = Modifier.fillMaxWidth()) {
            AppTextField(
                value = flat,
                onValueChange = { flat = it },
                changeBorder = false,
                label = {
                    Text(
                        text = "Квартира",
                        fontSize = 14.sp,
                        color = descriptionColor
                    )
                },
                columnModifier = Modifier
                    .weight(1f)
                    .padding(end = 17.5.dp),
                modifier = Modifier.fillMaxWidth()
            )
            AppTextField(
                value = entrance,
                onValueChange = { entrance = it },
                changeBorder = false,
                label = {
                    Text(
                        text = "Подъезд",
                        fontSize = 14.sp,
                        color = descriptionColor
                    )
                },
                columnModifier = Modifier
                    .weight(1f)
                    .padding(end = 17.5.dp),
                modifier = Modifier.fillMaxWidth()
            )
            AppTextField(
                value = floor,
                onValueChange = { floor = it },
                changeBorder = false,
                label = {
                    Text(
                        text = "Этаж",
                        fontSize = 14.sp,
                        color = descriptionColor
                    )
                },
                columnModifier = Modifier.weight(1f),
                modifier = Modifier.fillMaxWidth()
            )
        }
        Spacer(modifier = Modifier.height(16.dp))
        AppTextField(
            value = doorphone,
            onValueChange = { doorphone = it },
            changeBorder = false,
            label = {
                Text(
                    text = "Домофон",
                    fontSize = 14.sp,
                    color = descriptionColor
                )
            },
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(6.dp))
        Row(
            Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = "Сохранить этот адрес?",
                fontSize = 16.sp
            )
            Switch(
                checked = saveAddress,
                onCheckedChange = {
                    saveAddress = it
                },
                colors = SwitchDefaults.colors(
                    checkedTrackAlpha = 1f,
                    checkedThumbColor = Color.White,
                    checkedTrackColor = primary,
                    uncheckedThumbColor = Color.White,
                    uncheckedTrackColor = inputStroke,
                )
            )
        }
        AnimatedVisibility(visible = saveAddress) {
            AppTextField(
                value = addressName,
                onValueChange = { addressName = it },
                changeBorder = false,
                placeholder = {
                    Text(
                        text = "Название: например дом, работа",
                        fontSize = 14.sp,
                        color = captionColor
                    )
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 16.dp)
            )
        }
        Spacer(modifier = Modifier.height(18.dp))
        AppButton(
            onClick = {
                 if (saveAddress) {
                     DataSaver().saveAddress(
                         sharedPreferences,
                         AppAddress(
                             addressName,
                             address,
                             lat,
                             lon,
                             alt,
                             flat,
                             entrance,
                             floor,
                             doorphone
                         )
                     )
                 }

                onAddressChange(address)
            },
            label = "Подтвердить"
        )
    }
}
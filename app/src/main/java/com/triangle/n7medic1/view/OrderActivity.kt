package com.triangle.n7medic1.view

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.widget.Space
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.*
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsPressedAsState
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.location.LocationServices
import com.triangle.n7medic1.R
import com.triangle.n7medic1.common.CartService
import com.triangle.n7medic1.common.DataSaver
import com.triangle.n7medic1.common.EncryptDataSaver
import com.triangle.n7medic1.model.*
import com.triangle.n7medic1.ui.components.AppButton
import com.triangle.n7medic1.ui.components.AppTextButton
import com.triangle.n7medic1.ui.theme.*
import com.triangle.n7medic1.view.ui.components.*
import com.triangle.n7medic1.viewmodel.OrderViewModel
import kotlinx.coroutines.launch
import java.time.LocalDateTime

/*
Описание: Оформление заказа/1 пациент
Дата создания: 06.04.2023
Автор: Хасанов Альберт
 */
class OrderActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            N7medic1Theme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    ScreenContent()
                }
            }
        }
    }

    /*
    Описание: Содержание оформление заказа/1 пациент
    Дата создания: 06.04.2023
    Автор: Хасанов Альберт
     */
    @SuppressLint("CoroutineCreationDuringComposition")
    @OptIn(ExperimentalMaterialApi::class)
    @Composable
    fun ScreenContent() {
        val mContext = LocalContext.current
        val location = LocationServices.getFusedLocationProviderClient(mContext)
        val sharedPreferences = this.getSharedPreferences("shared", MODE_PRIVATE)
        val viewModel = ViewModelProvider(this)[OrderViewModel::class.java]
        val scope = rememberCoroutineScope()
        val bottomSheetState = rememberModalBottomSheetState(initialValue = ModalBottomSheetValue.Hidden, skipHalfExpanded = true)

        var isLoading by rememberSaveable { mutableStateOf(false) }
        var isErrorVisible by rememberSaveable { mutableStateOf(false) }

        val isSuccess by viewModel.isSuccess.observeAsState()
        LaunchedEffect(isSuccess) {
            if (isSuccess != null) {
                isLoading = false

                val intent = Intent(mContext, PayActivity::class.java)
                mContext.startActivity(intent)
            }
        }

        val errorMessage by viewModel.message.observeAsState()
        LaunchedEffect(errorMessage) {
            if (errorMessage != null) {
                isLoading = false
                isErrorVisible = true
            }
        }

        var selectedPatient: User? by rememberSaveable { mutableStateOf(null) }
        var selectedScreen by rememberSaveable { mutableStateOf("") }
        var address by rememberSaveable { mutableStateOf("") }
        var dateTime by rememberSaveable { mutableStateOf("") }
        var date by rememberSaveable { mutableStateOf(LocalDateTime.now()) }
        var phone by rememberSaveable { mutableStateOf("") }
        var comment by rememberSaveable { mutableStateOf("") }

        val selectedPatients: MutableList<User> = remember { mutableStateListOf() }
        val userList: MutableList<User> = remember { mutableStateListOf() }
        val cart: MutableList<CartItem> = remember { mutableStateListOf() }
        LaunchedEffect(Unit) {
            cart.addAll(CartService().loadCart(sharedPreferences))

            val uList: MutableList<User> = DataSaver().loadUsers(sharedPreferences)
            for (u in uList) {
                userList.add(
                    User(
                        u.id, u.firstName, u.patronymic, u.lastName, u.birthday, u.gender, u.image, cart
                    )
                )
            }

            val order = DataSaver().loadOrder(sharedPreferences)
            if (order != null) {
                if (userList.isNotEmpty()) {
                    selectedPatients.add(userList[0])
                }
                address = order.address
                dateTime = order.dateTime
                selectedPatients.addAll(order.selectedPatients)
                phone = order.phone
                comment = order.comment
            }
        }

        val addressInteractionSource = remember { MutableInteractionSource() }
        if (addressInteractionSource.collectIsPressedAsState().value) {
            isLoading = true
            if (ActivityCompat.checkSelfPermission(
                    mContext,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    mContext,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    mContext as Activity,
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ),
                    1
                )
            } else {
                viewModel.getAddress(location)
            }
        }

        val addr by viewModel.address.observeAsState()
        LaunchedEffect(addr) {
            if (addr != null) {
                isLoading = false

                selectedScreen = "address"
                scope.launch {
                    bottomSheetState.show()
                }
            }
        }

        val timeInteractionSource = remember { MutableInteractionSource() }
        if (timeInteractionSource.collectIsPressedAsState().value) {
            selectedScreen = "time"
            scope.launch {
                bottomSheetState.show()
            }
        }

        ModalBottomSheetLayout(
            sheetState = bottomSheetState,
            sheetShape = RoundedCornerShape(24.dp, 24.dp, 0.dp, 0.dp),
            sheetContent = {
                when(selectedScreen) {
                    "address" -> {
                        Log.d("TAG", "ScreenContent: ${viewModel.lat.value}")
                        AddressBottomSheet(
                            onAddressChange = {
                                address = it
                                scope.launch {
                                    bottomSheetState.hide()
                                }
                                saveCurrentOrder(sharedPreferences, Order(address, dateTime, selectedPatients, phone, comment))
                            },
                            addr = addr.toString(),
                            tLat = viewModel.lat.value.toString(),
                            tLon = viewModel.lon.value.toString(),
                            tAlt = viewModel.alt.value.toString(),
                        )
                    }
                    "time" -> {
                        TimeBottomSheet(
                            onDateTimeSelect = { time, ldt ->
                                dateTime = time
                                date = ldt
                                scope.launch {
                                    bottomSheetState.hide()
                                }
                                saveCurrentOrder(sharedPreferences, Order(address, dateTime, selectedPatients, phone, comment))
                            }
                        )
                    }
                    "patient" -> {
                        PatientSelectBottomSheet(
                            onPatientChange = {
                                if (selectedPatient != null) {
                                    val index = selectedPatients.indexOf(selectedPatient)
                                    selectedPatients.remove(it)
                                    selectedPatients.add(index, it)
                                } else {
                                    selectedPatients.add(it)
                                }

                                scope.launch {
                                    bottomSheetState.hide()
                                }

                                saveCurrentOrder(sharedPreferences, Order(address, dateTime, selectedPatients, phone, comment))
                            },
                            userList = userList
                        )
                    }
                    "" -> {
                        Spacer(modifier = Modifier.height(1.dp))
                    }
                }
            }
        ) {
            Scaffold(
                topBar = {
                    Column(modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 20.dp, top = 16.dp)
                        .background(Color.White)) {
                        Box(
                            modifier = Modifier
                                .size(32.dp)
                                .clip(MaterialTheme.shapes.medium)
                                .background(inputBG)
                                .clickable {
                                    val intent = Intent(mContext, HomeActivity::class.java)
                                    startActivity(intent)
                                }
                        ) {
                            Icon(
                                painter = painterResource(id = R.drawable.ic_back),
                                contentDescription = "",
                                modifier = Modifier
                                    .align(Alignment.Center),
                                tint = descriptionColor
                            )
                        }
                        Spacer(modifier = Modifier.height(height = 24.dp))
                        Text(
                            text = "Оформление заказа",
                            fontSize = 24.sp,
                            fontWeight = FontWeight.Bold
                        )
                    }
                },
            ) {
                Box(modifier = Modifier.padding(it)) {
                    Column(
                        Modifier
                            .fillMaxSize()
                            .verticalScroll(rememberScrollState())
                    ) {
                        Column(
                            Modifier
                                .fillMaxWidth()
                                .padding(horizontal = 20.dp)) {
                            Spacer(modifier = Modifier.height(32.dp))
                            AppTextField(
                                value = address,
                                onValueChange = {},
                                changeBorder = false,
                                readOnly = true,
                                label = {
                                    Text(
                                        text = "Адрес *",
                                        fontSize = 14.sp,
                                        color = descriptionColor
                                    )
                                },
                                placeholder = {
                                    Text(
                                        text = "Введите ваш адрес",
                                        fontSize = 15.sp,
                                        color = Color.Black
                                    )
                                },
                                interactionSource = addressInteractionSource,
                                modifier = Modifier.fillMaxWidth()
                            )
                            Spacer(modifier = Modifier.height(16.dp))
                            AppTextField(
                                value = dateTime,
                                onValueChange = {},
                                changeBorder = false,
                                readOnly = true,
                                label = {
                                    Text(
                                        text = "Дата и время*",
                                        fontSize = 14.sp,
                                        color = descriptionColor
                                    )
                                },
                                placeholder = {
                                    Text(
                                        text = "Выберите дату и время",
                                        fontSize = 15.sp,
                                        color = captionColor
                                    )
                                },
                                interactionSource = timeInteractionSource,
                                modifier = Modifier.fillMaxWidth()
                            )
                            Spacer(modifier = Modifier.height(32.dp))
                            Text(
                                buildAnnotatedString {
                                    append("Кто будет сдавать анализы? ")
                                    withStyle(style = SpanStyle(color = Color(0xFFFD3535))) {
                                        append("*")
                                    }
                                },
                                fontSize = 14.sp,
                                color = descriptionColor
                            )
                            Column(modifier = Modifier.fillMaxWidth()) {
                                if (selectedPatients.size == 1) {
                                    val patient = userList[0]
                                    AppTextField(
                                        value = "${patient.lastName} ${patient.firstName}",
                                        onValueChange = {},
                                        changeBorder = false,
                                        readOnly = true,
                                        trailingIcon = {
                                            IconButton(onClick = {
                                                selectedScreen = "patient"
                                                selectedPatient = patient

                                                scope.launch {
                                                    bottomSheetState.show()
                                                }
                                            }) {
                                                Icon(
                                                    painter = painterResource(id = R.drawable.ic_dropdown),
                                                    contentDescription = "",
                                                    tint = descriptionColor,
                                                )
                                            }
                                        },
                                        leadingIcon = {
                                            Image(
                                                painter = painterResource(id = if (patient.gender == "Мужской") R.drawable.male else R.drawable.female),
                                                contentDescription = "",
                                                modifier = Modifier.size(24.dp)
                                            )
                                        },
                                        modifier = Modifier.fillMaxWidth()
                                    )
                                    Spacer(modifier = Modifier.height(16.dp))
                                } else if (selectedPatients.size > 1) {
                                    for ((index, p) in selectedPatients.withIndex()) {
                                        MultiplePatientCard(
                                            user = p,
                                            cart = cart,
                                            onCartChange = { u, c ->
                                                selectedPatients.removeAt(index)
                                                selectedPatients.add(index, User(
                                                    u.id, u.firstName, u.patronymic, u.lastName, u.birthday, u.gender, u.image, c
                                                ))

                                                saveCurrentOrder(sharedPreferences, Order(address, dateTime, selectedPatients, phone, comment))
                                            },
                                            onChangeClick = {
                                                selectedScreen = "patient"
                                                selectedPatient = it

                                                scope.launch {
                                                    bottomSheetState.show()
                                                }
                                            },
                                            onRemoveClick = {
                                                selectedPatients.remove(it)

                                                saveCurrentOrder(sharedPreferences, Order(address, dateTime, selectedPatients, phone, comment))
                                            }
                                        )
                                        Spacer(modifier = Modifier.height(16.dp))
                                    }
                                }
                            }
                            AppButton(
                                onClick = {
                                    selectedScreen = "patient"
                                    selectedPatient = null

                                    scope.launch {
                                        bottomSheetState.show()
                                    }
                                },
                                label = "Добавить еще пациента",
                                border = BorderStroke(1.dp, primary),
                                style = TextStyle(
                                    fontWeight = FontWeight.Normal
                                ),
                                colors = ButtonDefaults.buttonColors(
                                    backgroundColor = Color.White,
                                    contentColor = primary
                                )
                            )
                            Spacer(modifier = Modifier.height(32.dp))
                            AppTextField(
                                value = phone,
                                onValueChange = {
                                    phone = it
                                    saveCurrentOrder(sharedPreferences, Order(address, dateTime, selectedPatients, phone, comment))
                                },
                                changeBorder = false,
                                label = {
                                    Text(
                                        text = "Телефон *",
                                        fontSize = 14.sp,
                                        color = descriptionColor
                                    )
                                },
                                modifier = Modifier.fillMaxWidth()
                            )
                            Spacer(modifier = Modifier.height(16.dp))
                            Column(Modifier.fillMaxWidth()) {
                                Row(
                                    Modifier.fillMaxWidth(),
                                    verticalAlignment = Alignment.CenterVertically,
                                    horizontalArrangement = Arrangement.SpaceBetween
                                ) {
                                    Text(
                                        text = "Комментарий",
                                        fontSize = 14.sp,
                                        color = descriptionColor
                                    )
                                    IconButton(
                                        onClick = {},
                                        modifier = Modifier.size(20.dp)
                                    ) {
                                        Icon(
                                            painter = painterResource(id = R.drawable.ic_mic),
                                            contentDescription = "",
                                        )
                                    }
                                }
                                Spacer(modifier = Modifier.height(4.dp))
                                AppTextField(
                                    value = comment,
                                    onValueChange = {
                                        comment = it
                                        saveCurrentOrder(sharedPreferences, Order(address, dateTime, selectedPatients, phone, comment))
                                    },
                                    changeBorder = false,
                                    placeholder = {
                                        Text(
                                            text = "Можете оставить свои пожелания",
                                            fontSize = 15.sp,
                                            color = captionColor
                                        )
                                    },
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .height(128.dp)
                                )
                            }
                        }
                        Spacer(modifier = Modifier.height(40.dp))
                        Column(
                            Modifier
                                .fillMaxWidth()
                                .background(inputBG)
                                .padding(20.dp, 16.dp)) {
                            Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
                                Text(
                                    "Оплата Apple Pay",
                                    fontSize = 15.sp
                                )
                                Icon(
                                    painter = painterResource(id = R.drawable.ic_right),
                                    contentDescription = "",
                                    modifier = Modifier
                                        .size(20.dp),
                                    tint = iconsColor
                                )
                            }
                            Spacer(modifier = Modifier.height(16.dp))
                            Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
                                Text(
                                    "Промокод",
                                    fontSize = 15.sp,
                                    color = captionColor
                                )
                                Icon(
                                    painter = painterResource(id = R.drawable.ic_right),
                                    contentDescription = "",
                                    modifier = Modifier
                                        .size(20.dp),
                                    tint = iconsColor
                                )
                            }
                            Spacer(modifier = Modifier.height(29.dp))
                            Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
                                var count = 0
                                var sum = 0

                                for (p in selectedPatients) {
                                    for (c in p.cart) {
                                        count += c.count
                                        sum += c.price.toInt() * c.count
                                    }
                                }

                                Text(
                                    text = "$count анализ",
                                    fontSize = 17.sp
                                )
                                Text(
                                    text = "$sum ₽",
                                    fontSize = 17.sp
                                )
                            }
                            Spacer(modifier = Modifier.height(12.dp))
                            AppButton(
                                onClick = {
                                    isLoading = true
                                    val users: MutableList<OrderPatient> = ArrayList()

                                    for (s in selectedPatients) {
                                        val uCart: MutableList<OrderItem> = ArrayList()
                                        for (c in s.cart) {
                                            uCart.add(OrderItem(c.id, c.price))
                                        }
                                        users.add(OrderPatient("${s.lastName} ${s.firstName}", uCart))
                                    }

                                    val serverOrder = ServerOrder(address, dateTime, phone, comment, "", users)

                                    viewModel.sendOrder(serverOrder, EncryptDataSaver(application).getToken())
                                },
                                label = "Заказать",
                                enabled = address.isNotBlank() && dateTime.isNotBlank() && selectedPatients.isNotEmpty() && phone.isNotBlank()
                            )
                            Spacer(modifier = Modifier.height(32.dp))
                        }
                    }
                }
            }
        }

        if (isLoading) {
            LoadingDialog()
        }

        if (isErrorVisible) {
            AlertDialog(
                onDismissRequest = { isErrorVisible = false },
                title = {
                    Text(text = "Ошибка", fontSize = 18.sp, fontWeight = FontWeight.SemiBold)
                },
                text = {
                    Text(text = errorMessage!!, fontSize = 16.sp)
                },
                buttons = {
                    AppTextButton(
                        onClick = {
                            isErrorVisible = false
                        },
                        label = "Ок"
                    )
                }
            )
        }
    }

    /*
    Описание: Сохранение заказа локально
    Дата создания: 10.04.2023
    Автор: Хасанов Альберт
     */
    fun saveCurrentOrder(shared: SharedPreferences, order: Order) {
        DataSaver().saveOrder(shared, order)
    }
}
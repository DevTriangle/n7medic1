package com.triangle.n7medic1.view

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.rememberPagerState
import com.triangle.n7medic1.R
import com.triangle.n7medic1.common.DataSaver
import com.triangle.n7medic1.ui.components.AppTextButton
import com.triangle.n7medic1.ui.components.OnboardComponent
import com.triangle.n7medic1.ui.theme.Lato
import com.triangle.n7medic1.ui.theme.N7medic1Theme
import kotlinx.coroutines.flow.collect

/*
Описание: Активити приветственного экрана
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
class OnboardActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            N7medic1Theme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    ScreenContent()
                }
            }
        }
    }

    /*
    Описание: Содержание приветственного экрана
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    @OptIn(ExperimentalPagerApi::class)
    @Composable
    fun ScreenContent() {
        val mContext = LocalContext.current
        val sharedPreferences = this.getSharedPreferences("shared", MODE_PRIVATE)
        val pagerState = rememberPagerState()

        val imagesList = listOf(
            painterResource(id = R.drawable.onboard_1),
            painterResource(id = R.drawable.onboard_2),
            painterResource(id = R.drawable.onboard_3),
        )
        val screenList = listOf(
            mapOf(
                "title" to "Анализы",
                "desc" to "Экспресс сбор и получение проб",
                "image" to imagesList[0],
            ),
            mapOf(
                "title" to "Уведомления",
                "desc" to "Вы быстро узнаете о результатах",
                "image" to imagesList[1],
            ),
            mapOf(
                "title" to "Мониторинг",
                "desc" to "Наши врачи всегда наблюдают за вашими показателями здоровья",
                "image" to imagesList[2],
            ),
        )

        var skipText by rememberSaveable { mutableStateOf("Пропустить") }

        LaunchedEffect(pagerState) {
            snapshotFlow { pagerState.currentPage }.collect() {
                skipText = if (it < 2) "Пропустить"
                else "Завершить"
            }
        }

        Column(modifier = Modifier.fillMaxSize()) {
            Row(
                Modifier
                    .fillMaxWidth()
                    .padding(top = 5.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                AppTextButton(
                    onClick = {
                        saveSecondLaunch(sharedPreferences)

                        val intent = Intent(mContext, LoginActivity::class.java)
                        startActivity(intent)
                    },
                    label = skipText,
                    style = TextStyle(
                        fontSize = 20.sp,
                        fontFamily = Lato,
                        fontWeight = FontWeight.Bold
                    ),
                    modifier = Modifier.padding(start = 30.dp)
                )
                Image(
                    painter = painterResource(id = R.drawable.onboard_shape),
                    contentDescription = "",
                )
            }
            HorizontalPager(
                count = 3,
                state = pagerState,
                modifier = Modifier.testTag("pager")
            ) { page ->
                OnboardComponent(
                    info = screenList[page],
                    currentIndex = page
                )
            }
        }
    }

    private fun saveSecondLaunch(sharedPreferences: SharedPreferences) {
        DataSaver().saveSecondLaunch(sharedPreferences)
    }
}
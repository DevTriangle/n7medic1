package com.triangle.n7medic1.view.ui.components

import android.content.Intent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.triangle.n7medic1.R
import com.triangle.n7medic1.model.User
import com.triangle.n7medic1.ui.components.AppButton
import com.triangle.n7medic1.ui.theme.*
import com.triangle.n7medic1.view.ManageCardActivity
import kotlinx.coroutines.launch
import java.time.LocalDateTime

/*
Описание: Экран выбора пациента
Дата создания: 10.04.2023
Автор: Хасанов Альберт
 */
@Composable
fun PatientSelectBottomSheet(
    onPatientChange: (User) -> Unit,
    userList: MutableList<User>
) {
    val mContext = LocalContext.current
    var selectedPatient: User? by rememberSaveable { mutableStateOf(null) }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(20.dp, 24.dp)
    ) {
        Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
            Text(
                text = "Дата и время",
                fontFamily = SSPro,
                fontSize = 20.sp,
                fontWeight = FontWeight.SemiBold,
                modifier = Modifier.fillMaxWidth(0.7f)
            )
            Box(
                modifier = Modifier
                    .size(24.dp)
                    .clip(CircleShape)
                    .background(inputBG)
                    .clickable {}
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_close),
                    contentDescription = "",
                    modifier = Modifier
                        .align(Alignment.Center),
                    tint = descriptionColor
                )
            }
        }
        Spacer(modifier = Modifier.height(24.dp))
        for (u in userList.distinct()) {
            PatientCard(
                user = u,
                selected = selectedPatient == u,
                onSelect = {
                    selectedPatient = u
                }
            )
            Spacer(modifier = Modifier.height(16.dp))
        }
        Spacer(modifier = Modifier.height(8.dp))
        AppButton(
            onClick = {
                val intent = Intent(mContext, ManageCardActivity::class.java)
                mContext.startActivity(intent)
            },
            label = "Добавить пациента",
            border = BorderStroke(1.dp, primary),
            style = TextStyle(
                fontWeight = FontWeight.Normal
            ),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color.White,
                contentColor = primary
            )
        )
        Spacer(modifier = Modifier.height(40.dp))
        AppButton(
            onClick = {
                onPatientChange(selectedPatient!!)
            },
            enabled = selectedPatient != null,
            label = "Подтвердить"
        )
    }
}
package com.triangle.n7medic1.ui.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.indication
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.NonRestartableComposable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.triangle.n7medic1.ui.theme.SSPro
import com.triangle.n7medic1.ui.theme.inputBG
import com.triangle.n7medic1.ui.theme.primary
import com.triangle.n7medic1.ui.theme.primaryVariant

/*
Описание: Текстовая кнопка
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
@Composable
fun AppTextButton(
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    elevation: ButtonElevation? = ButtonDefaults.elevation(0.dp, 0.dp, 0.dp, 0.dp, 0.dp),
    shape: Shape = MaterialTheme.shapes.medium,
    border: BorderStroke? = null,
    colors: ButtonColors = ButtonDefaults.textButtonColors(
        contentColor = primaryVariant
    ),
    contentPadding: PaddingValues = ButtonDefaults.TextButtonContentPadding,
    label: String,
    style: TextStyle = LocalTextStyle.current
) = Button(
    onClick = onClick,
    modifier = modifier,
    enabled = enabled,
    interactionSource = interactionSource,
    elevation = elevation,
    shape = shape,
    border = border,
    colors = colors,
    contentPadding = contentPadding,
    content = {
        Text(text = label, style = style)
    }
)

/*
Описание: Текстовая кнопка
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
@Composable
fun AppButton(
    onClick: () -> Unit,
    modifier: Modifier = Modifier.fillMaxWidth(),
    enabled: Boolean = true,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    elevation: ButtonElevation? = ButtonDefaults.elevation(0.dp, 0.dp, 0.dp, 0.dp, 0.dp),
    shape: Shape = MaterialTheme.shapes.medium,
    border: BorderStroke? = null,
    colors: ButtonColors = ButtonDefaults.buttonColors(
        contentColor = Color.White, disabledContentColor = Color.White,
        backgroundColor = primary, disabledBackgroundColor = Color(0xFFC9D4FB)
    ),
    contentPadding: PaddingValues = PaddingValues(16.dp),
    label: String,
    style: TextStyle = LocalTextStyle.current.copy(fontWeight = FontWeight.SemiBold, fontFamily = SSPro)
) = Button(
    onClick = onClick,
    modifier = modifier,
    enabled = enabled,
    interactionSource = interactionSource,
    elevation = elevation,
    shape = shape,
    border = border,
    colors = colors,
    contentPadding = contentPadding,
    content = {
        Text(text = label, style = style, fontSize = 17.sp)
    }
)

/*
Описание: Текстовая кнопка
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
@Composable
fun PasswordButton(
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    label: Int
) {
    Box(
        modifier = modifier
            .size(80.dp)
            .clip(CircleShape)
            .background(inputBG)
            .clickable { onClick() }
    ) {
        Text(
            text = label.toString(),
            fontSize = 24.sp,
            fontWeight = FontWeight.SemiBold,
            fontFamily = SSPro,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .align(Alignment.Center)
        )
    }
}
package com.triangle.n7medic1.common

import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.triangle.n7medic1.model.Analysis
import com.triangle.n7medic1.model.News
import com.triangle.n7medic1.model.ServerOrder
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.*

/*
Описание: Интерфейс для обращения к API
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
interface ApiService {

    /*
    Описание: Метод для отправки кода на email
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    @POST("sendCode")
    @Headers(
        "accept: application/json"
    )
    suspend fun sendCode(@Header("email") email: String): Response<JsonObject>

    /*
    Описание: Метод для авторизации пользователя
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    @POST("signin")
    @Headers(
        "accept: application/json"
    )
    suspend fun signIn(@Header("email") email: String, @Header("code") code: String): Response<JsonObject>

    /*
    Описание: Метод для создания карты пользователя
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    @POST("createProfile")
    @Headers(
        "accept: application/json",
        "Content-Type: application/json"
    )
    suspend fun createProfile(@Header("Authorization") auth: String, @Body data: Map<String, String>): Response<JsonObject>

    /*
    Описание: Метод для обновления карты пользователя
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    @PUT("updateProfile")
    @Headers(
        "accept: application/json",
        "Content-Type: application/json"
    )
    suspend fun updateProfile(@Header("Authorization") auth: String, @Body data: Map<String, String>): Response<JsonObject>

    /*
    Описание: Метод для получения новостей с сервера
    Дата создания: 06.04.2023
    Автор: Хасанов Альберт
    */
    @GET("news")
    @Headers(
        "accept: application/json",
    )
    suspend fun loadNews(): Response<MutableList<News>>

    /*
    Описание: Метод для получения каталога с сервера
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
    */
    @GET("catalog")
    @Headers(
        "accept: application/json",
    )
    suspend fun loadCatalog(): Response<MutableList<Analysis>>

    /*
   Описание: Метод для отпраки заказа на сервер
   Дата создания: 04.04.2023
   Автор: Хасанов Альберт
   */
    @POST("order")
    @Headers(
        "accept: application/json",
    )
    suspend fun sendOrder(@Header("Authorization") auth: String, @Body order: ServerOrder): Response<JsonObject>

    companion object {
        var apiService: ApiService? = null

        /*
        Описание: Метод инициализации Retrofit
        Дата создания: 04.04.2023
        Автор: Хасанов Альберт
         */
        fun getInstance(): ApiService {
            if (apiService == null) {
                apiService = Retrofit.Builder()
                    .baseUrl("https://medic.madskill.ru/api/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(ApiService::class.java)
            }

            return apiService!!
        }
    }
}
package com.triangle.n7medic1.common

import android.app.Application
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import javax.crypto.EncryptedPrivateKeyInfo

/*
Описание: Класс для сохранения и шифрования данных на устройстве
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
class EncryptDataSaver(application: Application) {
    private val mainAliasKey by lazy {
        val keyParam = MasterKeys.AES256_GCM_SPEC
        MasterKeys.getOrCreate(keyParam)
    }

    private val eSharedPreferences = EncryptedSharedPreferences.create(
        "shared",
        mainAliasKey,
        application,
        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM,
    )

    /*
    Описание: Метод для сохранения токена
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    fun saveToken(token: String) {
        with(eSharedPreferences.edit()) {
            putString("token", token)
            apply()
        }
    }

    /*
    Описание: Метод для получения токена
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    fun getToken(): String {
        return eSharedPreferences.getString("token", "")!!
    }

    /*
    Описание: Метод для сохранения пароля
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    fun savePassword(password: String) {
        with(eSharedPreferences.edit()) {
            putString("password", password)
            apply()
        }
    }

    /*
    Описание: Метод для загрузки пароля
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    fun getPassword(): String {
       return eSharedPreferences.getString("password", "")!!
    }
}
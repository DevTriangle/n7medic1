package com.triangle.n7medic1.common

import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.triangle.n7medic1.model.AppAddress
import com.triangle.n7medic1.model.Order
import com.triangle.n7medic1.model.User

/*
Описание: Класс для сохранения данных на устройстве
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
class DataSaver {

    /*
    Описание: Метод для сохранения информации о прохождении приветственного экрана
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    fun saveSecondLaunch(sharedPreferences: SharedPreferences) {
        with(sharedPreferences.edit()) {
            putBoolean("isFirstLaunch", false)
            apply()
        }
    }

    /*
    Описание: Метод для сохранения информации об установке пароля
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    fun savePasswordSet(sharedPreferences: SharedPreferences) {
        with(sharedPreferences.edit()) {
            putBoolean("isPasswordSet", true)
            apply()
        }
    }

    /*
   Описание: Метод для сохранения информации о пользователях
   Дата создания: 04.04.2023
   Автор: Хасанов Альберт
    */
    fun saveUsers(sharedPreferences: SharedPreferences, userList: MutableList<User>) {
        val usersJson = Gson().toJson(userList)

        with(sharedPreferences.edit()) {
            putString("users", usersJson)
            apply()
        }
    }

    /*
   Описание: Метод для получения всех пользователей
   Дата создания: 04.04.2023
   Автор: Хасанов Альберт
    */
    fun loadUsers(sharedPreferences: SharedPreferences): MutableList<User> {
        val userList: ArrayList<User> = ArrayList()
        val usersJson = Gson().fromJson(sharedPreferences.getString("users", "[]"), JsonArray::class.java)

        for (u in usersJson) {
            val user = u.asJsonObject

            userList.add(
                User(
                    user.get("id").asInt,
                    user.get("firstname").asString,
                    user.get("middlename").asString,
                    user.get("lastname").asString,
                    user.get("bith").asString,
                    user.get("pol").asString,
                    user.get("image") as String?
                )
            )
        }

        return userList.toMutableList()
    }

    /*
   Описание: Метод для сохранения адреса
   Дата создания: 04.04.2023
   Автор: Хасанов Альберт
    */
    fun saveAddress(sharedPreferences: SharedPreferences, address: AppAddress) {
        val jsonAddress = Gson().toJson(address)

        with(sharedPreferences.edit()) {
            putString("address", jsonAddress)
            apply()
        }
    }

    /*
    Описание: Метод для сохранения текщуего заказа
    Дата создания: 10.04.2023
    Автор: Хасанов Альберт
   */
    fun saveOrder(sharedPreferences: SharedPreferences, order:Order) {
        val jsonOrder = Gson().toJson(order)

        with(sharedPreferences.edit()) {
            putString("order", jsonOrder)
            apply()
        }
    }

    /*
    Описание: Метод для получения текщуего заказа
    Дата создания: 10.04.2023
    Автор: Хасанов Альберт
     */
    fun loadOrder(sharedPreferences: SharedPreferences): Order? {
        val jsonOrder = sharedPreferences.getString("order", null)

        return Gson().fromJson(jsonOrder, Order::class.java)
    }
}
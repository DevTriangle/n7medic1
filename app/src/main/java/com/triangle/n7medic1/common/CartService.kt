package com.triangle.n7medic1.common

import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.triangle.n7medic1.model.CartItem

/*
Описание: Класс для сохранения и получения данных корзины
Дата создания: 06.04.2023
Автор: Хасанов Альберт
 */
class CartService {

    /*
    Описание: Метод для получения данных корзины
    Дата создания: 06.04.2023
    Автор: Хасанов Альберт
     */
    fun loadCart(sharedPreferences: SharedPreferences): MutableList<CartItem> {
        val cart: MutableList<CartItem> = ArrayList<CartItem>()
        val cartJson = Gson().fromJson(sharedPreferences.getString("cart", "[]"), JsonArray::class.java)

        for (c in cartJson) {
            val item = c.asJsonObject

            cart.add(
                CartItem(
                    item.get("id").asInt,
                    item.get("name").asString,
                    item.get("price").asString,
                    item.get("count").asInt,
                )
            )
        }

        return cart
    }

    /*
    Описание: Класс для сохранения данных корзины
    Дата создания: 06.04.2023
    Автор: Хасанов Альберт
     */
    fun saveCart(sharedPreferences: SharedPreferences, cart: MutableList<CartItem>) {
        val cartJson = Gson().toJson(cart)

        with(sharedPreferences.edit()) {
            putString("cart", cartJson)
            apply()
        }
    }
}
package com.triangle.n7medic1.common

import com.google.android.exoplayer2.Format
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.triangle.n7medic1.model.Analysis
import com.triangle.n7medic1.model.News
import com.triangle.n7medic1.model.ServerOrder
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.*

/*
Описание: Интерфейс для обращения к API геокодирования
Дата создания: 10.04.2023
Автор: Хасанов Альберт
 */
interface GeoApiService {

    /*
    Описание: Метод для получения адреса
    Дата создания: 10.04.2023
    Автор: Хасанов Альберт
     */
    @GET("search")
    @Headers(
        "accept: application/json"
    )
    suspend fun getAddress(@Query("q") q: String, @Query("format") format: String): Response<JsonArray>

    companion object {
        var apiService: GeoApiService? = null

        /*
        Описание: Метод инициализации Retrofit
        Дата создания: 10.04.2023
        Автор: Хасанов Альберт
         */
        fun getInstance(): GeoApiService {
            if (apiService == null) {
                apiService = Retrofit.Builder()
                    .baseUrl("https://nominatim.openstreetmap.org/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(GeoApiService::class.java)
            }

            return apiService!!
        }
    }
}
package com.triangle.n7medic1.viewmodel

import android.content.ContentValues.TAG
import android.graphics.Bitmap
import android.media.session.MediaSession.Token
import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import com.triangle.n7medic1.common.ApiService
import com.triangle.n7medic1.model.User
import kotlinx.coroutines.launch
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import kotlin.math.log

/*
Описание: Класс ViewModel для создания и изменения карт пациентов
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
class UserViewModel: ViewModel() {
    val message = MutableLiveData<String>()
    val isSuccessCreateCard = MutableLiveData<Boolean>()

    val newUser = MutableLiveData<User>()

    val isVideo = MutableLiveData(false)
    var image = MutableLiveData<Bitmap>()
    var video = MutableLiveData<Uri>()

    /*
    Описание: Метод для создания карты пользователя
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    fun createProfile(
        firstName: String,
        patronymic: String,
        lastName: String,
        birthday: String,
        gender: String,
        token: String
    ) {
        message.value = null
        isSuccessCreateCard.value = null
        newUser.value = null

        val apiService = ApiService.getInstance()

        viewModelScope.launch {
            try {
                val data = mapOf(
                    "firstname" to firstName,
                    "lastname" to lastName,
                    "middlename" to patronymic,
                    "bith" to birthday,
                    "pol" to gender,
                    "image" to ""
                )

                val t = token.replace("\"","")

                val json = apiService.createProfile("Bearer $t", data)

                if (json.code() == 200) {
                    newUser.value = User(
                        json.body()?.get("id")!!.asInt,
                        json.body()?.get("firstname")!!.asString,
                        json.body()?.get("middlename")!!.asString,
                        json.body()?.get("lastname")!!.asString,
                        json.body()?.get("bith")!!.asString,
                        json.body()?.get("pol")!!.asString,
                    )


                } else {
                    message.value = "${json.code()} - ${json.body()?.get("errors")?.asJsonArray?.get(0)}"
                }
            } catch (e: Exception) {
                message.value = "Проверьте подключение к сети! (${e.message})"
            }
        }
    }

    /*
    Описание: Метод для обновления карты пользователя
    Дата создания: 06.04.2023
    Автор: Хасанов Альберт
     */
    fun updateProfile(
        firstName: String,
        patronymic: String,
        lastName: String,
        birthday: String,
        gender: String,
        token: String
    ) {
        message.value = null
        isSuccessCreateCard.value = null
        newUser.value = null

        val apiService = ApiService.getInstance()

        viewModelScope.launch {
            try {
                val data = mapOf(
                    "firstname" to firstName,
                    "lastname" to lastName,
                    "middlename" to patronymic,
                    "bith" to birthday,
                    "pol" to gender,
                )

                val t = token.replace("\"","")

                Log.d(TAG, "updateProfile: $t")

                val json = apiService.updateProfile("Bearer $t", data)

                if (json.code() == 200) {
                    newUser.value = User(
                        json.body()?.get("id")!!.asInt,
                        json.body()?.get("firstname")!!.asString,
                        json.body()?.get("middlename")!!.asString,
                        json.body()?.get("lastname")!!.asString,
                        json.body()?.get("bith")!!.asString,
                        json.body()?.get("pol")!!.asString,
                    )
                } else {
                    message.value = "${json.code()} - ${json.body()?.get("errors")?.asJsonArray?.get(0)}"
                }
            } catch (e: Exception) {
                message.value = "Проверьте подключение к сети! (${e.message})"
            }
        }
    }
}
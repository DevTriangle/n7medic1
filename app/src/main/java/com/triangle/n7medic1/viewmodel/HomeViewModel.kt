package com.triangle.n7medic1.viewmodel

import android.annotation.SuppressLint
import android.view.View
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.triangle.n7medic1.common.ApiService
import com.triangle.n7medic1.model.Analysis
import com.triangle.n7medic1.model.News
import kotlinx.coroutines.launch

/*
Описание: Класс ViewModel для получения новостей и каталога
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
@SuppressLint("MutableCollectionMutableState")
class HomeViewModel: ViewModel() {
    val message = MutableLiveData<String>()
    val isSuccessLoadNews = MutableLiveData<Boolean>()

    private val _news: MutableList<News> = mutableStateListOf()
    val news: List<News> by mutableStateOf(_news)

    private val _catalog: MutableList<Analysis> = mutableStateListOf()
    val catalog: List<Analysis> by mutableStateOf(_catalog)

    val isSuccessLoadCatalog = MutableLiveData<Boolean>()

    val categories = listOf(
        "Популярные",
        "Covid",
        "Комплексные",
        "Чекапы",
        "Биохимия",
        "Гормоны",
        "Иммунитет",
        "Витамины",
        "Аллергены",
        "Анализ крови",
        "Анализ мочи",
        "Анализ кала",
        "Только в клинике",
    )

    /*
    Описание: Метод для получения новостей с сервера
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    fun loadNews() {
        message.value = null
        isSuccessLoadNews.value = null
        _news.clear()

        val apiService = ApiService.getInstance()

        viewModelScope.launch {
            try {
                val json = apiService.loadNews()

                if (json.code() == 200) {
                    _news.addAll(json.body()!!)
                    isSuccessLoadNews.value = true
                } else {
                    message.value = "${json.code()} ${json.message()}"
                    isSuccessLoadNews.value = false
                }
            } catch (e: Exception) {
                isSuccessLoadNews.value = false
                message.value = "Проверьте подключение к сети! (${e.message})"
            }
        }
    }

    /*
    Описание: Метод для получения каталога с сервера
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    fun loadCatalog() {
        message.value = null
        isSuccessLoadCatalog.value = null
        _catalog.clear()

        val apiService = ApiService.getInstance()

        viewModelScope.launch {
            try {
                val json = apiService.loadCatalog()

                if (json.code() == 200) {
                    _catalog.addAll(json.body()!!)
                    isSuccessLoadCatalog.value = true
                } else {
                    message.value = "${json.code()} ${json.message()}"
                    isSuccessLoadCatalog.value = false
                }
            } catch (e: Exception) {
                isSuccessLoadCatalog.value = false
                message.value = "Проверьте подключение к сети! (${e.message})"
            }
        }
    }
}
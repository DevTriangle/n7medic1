package com.triangle.n7medic1.viewmodel

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.pm.PackageManager
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.Priority
import com.triangle.n7medic1.common.ApiService
import com.triangle.n7medic1.common.GeoApiService
import com.triangle.n7medic1.model.ServerOrder
import com.triangle.n7medic1.model.User
import kotlinx.coroutines.launch

/*
Описание: Класс ViewModel для создания заказа
Дата создания: 06.04.2023
Автор: Хасанов Альберт
 */
class OrderViewModel: ViewModel() {
    val message = MutableLiveData<String>()
    val isSuccess = MutableLiveData<Boolean>()

    val address = MutableLiveData<String>()
    val lat = MutableLiveData<String>()
    val lon = MutableLiveData<String>()
    val alt = MutableLiveData<String>()

    /*
    Описание: Метод для оправки заказа на сервер
    Дата создания: 10.04.2023
    Автор: Хасанов Альберт
     */
    fun sendOrder(
        order: ServerOrder,
        token: String
    ) {
        message.value = null
        isSuccess.value = null

        val apiService = ApiService.getInstance()

        viewModelScope.launch {
            try {
                val t = token.replace("\"","")
                val json = apiService.sendOrder("Bearer $t", order)

                if (json.code() == 200) {
                    isSuccess.value = true
                } else {
                    isSuccess.value = false
                    message.value = "Проверьте подключение к сети! (${json.message()})"
                }
            } catch (e: Exception) {
                isSuccess.value = false
                message.value = "Проверьте подключение к сети! (${e.message})"
            }
        }
    }

    /*
    Описание: Метод для получения адреса
    Дата создания: 10.04.2023
    Автор: Хасанов Альберт
    */
    @SuppressLint("MissingPermission")
    fun getAddress(location: FusedLocationProviderClient) {
        message.value = null
        address.value = null
        lat.value = null
        lon.value = null
        alt.value = null

        val apiService = GeoApiService.getInstance()
        location.getCurrentLocation(Priority.PRIORITY_HIGH_ACCURACY, null).addOnCompleteListener {
            if (it.isComplete) {
                lat.value = it.result.latitude.toString()
                lon.value = it.result.longitude.toString()
                alt.value = it.result.altitude.toString()

                Log.d("TAG", "!!!!: ${lat.value}")

                viewModelScope.launch {
                    try {
                        val json = apiService.getAddress("${lat.value} ${lon.value}", "json")

                        if (json.code() == 200) {
                            address.value = json.body()!!.asJsonArray[0].asJsonObject.get("display_name").toString()
                        } else {
                            message.value = json.message()
                        }
                    } catch (e: Exception) {
                        message.value = "Проверьте подключение к сети! (${e.message})"
                    }
                }
            }
        }
    }
}
package com.triangle.n7medic1.viewmodel

import android.provider.ContactsContract.CommonDataKinds.Email
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.triangle.n7medic1.common.ApiService
import kotlinx.coroutines.launch

/*
Описание: Класс ViewModel для авторизации пользователя
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
class LoginViewModel: ViewModel() {
    val message = MutableLiveData<String>()
    val isSuccessSendCode = MutableLiveData<Boolean>()
    val token = MutableLiveData<String>()

    /*
    Описание: Метод для отправки кода на email
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    fun sendCode(email: String) {
        message.value = null
        isSuccessSendCode.value = null

        val apiService = ApiService.getInstance()

        viewModelScope.launch {
            try {
                val json = apiService.sendCode(email)

                if (json.code() == 200) {
                    isSuccessSendCode.value = true
                } else {
                    isSuccessSendCode.value = false
                    message.value = "${json.code()} - ${json.body()?.get("errors")?.asJsonArray?.get(0)}"
                }
            } catch (e: Exception) {
                isSuccessSendCode.value = false
                message.value = "Проверьте подключение к сети! (${e.message})"
            }
        }
    }

    /*
    Описание: Метод для авторизации пользователя
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    fun signIn(email: String, code: String) {
        message.value = null
        token.value = null

        val apiService = ApiService.getInstance()

        viewModelScope.launch {
            try {
                val json = apiService.signIn(email, code)

                if (json.code() == 200) {
                    token.value = json.body()?.get("token").toString()
                } else {
                    message.value = "${json.code()} - ${json.body()?.get("errors")?.asJsonArray?.get(0)}"
                }
            } catch (e: Exception) {
                message.value = "Проверьте подключение к сети! (${e.message})"
            }
        }
    }
}
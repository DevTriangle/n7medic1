package com.triangle.n7medic1

import android.content.Context.MODE_PRIVATE
import androidx.activity.compose.setContent
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.triangle.n7medic1.ui.theme.N7medic1Theme
import com.triangle.n7medic1.view.OnboardActivity

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Rule

/*
Описание: Класс тестов
Дата создания: 04.04.2023
Автор: Хасанов Альберт
 */
@RunWith(AndroidJUnit4::class)
class PagerTest {

    @get:Rule
    val testRule = createAndroidComposeRule<OnboardActivity>()

    /*
    Описание: Тест "Изображение и текста из очереди извлекается правильно (в порядке добавления в очередь)."
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    @Test
    fun pagerQueueTest() {
        testRule.activity.setContent {
            N7medic1Theme {
                testRule.activity.ScreenContent()
            }
        }

        testRule.onNodeWithText("Анализы").assertIsDisplayed()
        testRule.onNodeWithTag("pager").performTouchInput { swipeLeft() }
        testRule.onNodeWithText("Уведомления").assertIsDisplayed()
        testRule.onNodeWithTag("pager").performTouchInput { swipeLeft() }
        testRule.onNodeWithText("Мониторинг").assertIsDisplayed()
    }

    /*
    Описание: Тест "Корректное извлечение элементов из очереди (количествоэлементов в очереди уменьшается на единицу)."
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    @Test
    fun pagerQueue2Test() {
        testRule.activity.setContent {
            N7medic1Theme {
                testRule.activity.ScreenContent()
            }
        }

        testRule.onNodeWithText("Анализы").assertIsDisplayed()
        testRule.onNodeWithTag("pager").performTouchInput { swipeLeft() }
        testRule.onNodeWithText("Уведомления").assertIsDisplayed()
        testRule.onNodeWithTag("pager").performTouchInput { swipeLeft() }
        testRule.onNodeWithText("Мониторинг").assertIsDisplayed()
    }

    /*
    Описание: Тест "В случае, когда в очереди несколько картинок,
                устанавливается правильная надпись на кнопке."
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    @Test
    fun pagerButtonTest() {
        testRule.activity.setContent {
            N7medic1Theme {
                testRule.activity.ScreenContent()
            }
        }

        testRule.onNodeWithText("Пропустить").assertIsDisplayed()
        testRule.onNodeWithTag("pager").performTouchInput { swipeLeft() }
        testRule.onNodeWithText("Пропустить").assertIsDisplayed()
        testRule.onNodeWithTag("pager").performTouchInput { swipeLeft() }
        testRule.onNodeWithText("Завершить").assertIsDisplayed()
        testRule.onNodeWithTag("pager").performTouchInput { swipeRight() }
        testRule.onNodeWithText("Пропустить").assertIsDisplayed()
    }

    /*
    Описание: Тест "Случай, когда в очереди осталось только одно изображение,
                надпись на кнопке должна измениться на "Завершить"
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    @Test
    fun pagerEndTest() {
        testRule.activity.setContent {
            N7medic1Theme {
                testRule.activity.ScreenContent()
            }
        }

        testRule.onNodeWithTag("pager").performTouchInput { swipeLeft() }
        testRule.onNodeWithTag("pager").performTouchInput { swipeLeft() }
        testRule.onNodeWithText("Завершить").assertIsDisplayed()
    }

    /*
    Описание: Тест "Если очередь пустая и пользователь нажал на кнопку
        “Завершить”, происходит открытие экрана «Вход и регистрация/не
        заполнено» приложения. Если очередь не пустая – переход отсутствует."
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    @Test
    fun pagerSkipTest() {
        testRule.activity.setContent {
            N7medic1Theme {
                testRule.activity.ScreenContent()
            }
        }

        testRule.onNodeWithTag("pager").performTouchInput { swipeLeft() }
        testRule.onNodeWithTag("pager").performTouchInput { swipeLeft() }
        testRule.onNodeWithText("Завершить").performClick()

        testRule.onNodeWithTag("login").assertIsDisplayed()
    }

    /*
    Описание: Тест "Наличие вызова метода сохранения флага об успешном
                    прохождении приветствия пользователем."
    Дата создания: 04.04.2023
    Автор: Хасанов Альберт
     */
    @Test
    fun pagerSaveDataTest() {
        testRule.activity.setContent {
            N7medic1Theme {
                testRule.activity.ScreenContent()
            }
        }

        testRule.onNodeWithText("Пропустить").performClick()

        val sharedPreferences = testRule.activity.getSharedPreferences("shared", MODE_PRIVATE)
        val isFirstLaunch = sharedPreferences.getBoolean("isFirstLaunch", false)

        assertEquals(isFirstLaunch, false)
    }
}